<?php if (!defined('DATATABLES')) exit(); // Ensure being used in DataTables env.

/*
 * DB connection script for Editor
 * Created by http://editor.datatables.net/generator
 */

// Enable error reporting for debugging (remove for production)
error_reporting(E_ALL);
ini_set('display_errors', '1');

/*
 * Edit the following with your database connection options
 */
/*
$sql_details = array(
	"type" => "Mysql",
	"user" => "admin",
	"pass" => "cisco",
	"host" => "100.127.250.1",
	"port" => "",
	"db"   => "aci",
	"dsn"  => "charset=utf8"
);
*/
$sql_details = array(
	"type" => "Postgres",
	"user" => "aci_user",
	"pass" => "cisco",
	"host" => "100.127.250.1",
	"port" => "5432",
	"db"   => "aci",
	"dsn"  => ""
);
/*
$sql_details = array(
	"type" => "Sqlserver",
	"user" => "sa",
	"pass" => "C1sc0123",
	"host" => "100.127.250.1",
	"port" => "1402",
	"db"   => "saq",
	"dsn"  => ""
);
*/
