
/*
 * Editor client script for DB table ipplan
 * Created by http://editor.datatables.net/generator
 */

(function($){

$(document).ready(function() {
	var editor = new $.fn.dataTable.Editor( {
		ajax: 'php/table.ipplan.php',
		table: '#ipplan',
		fields: [
			{
				"label": "TENANT:",
				"name": "tenant"
			},
			{
				"label": "VRF_NAME:",
				"name": "vrf_name"
			},
			{
				"label": "VRF_ACI_NAME:",
				"labelInfo": "Correspond au nom de la VRF avec suffixe _VRF",
				"name": "vrf_aci_name",
				"def": "_VRF"
			},
			{
				"label": "BRIDGE_DOMAIN:",
				"name": "bd",
				"def": "_BD"
			},
			{
				"label": "SUBNET:",
				"name": "subnet"
			},
			{
				"label": "ANYCAST_GATEWAY:",
				"name": "anycast_gateway"
			},
			{
				"label": "APPLICATION PROFILE NAME:",
				"name": "appname",
				"def": "_APP"
			},
			{
				"label": "EPG:",
				"name": "epg",
				"def": "_EPG"
			},
			{
				"label": "VLAN_ID:",
				"name": "vlan_id",
				"def": "0000"
			},
			{
				"label": "VLAN_NAME:",
				"name": "vlan_name"
			},
			{
				"label": "FW_ZONE_NAME:",
				"name": "fw_zone"
			},
			{
				"label": "L3OUT_NAME:",
				"name": "l3out_name",
				"def": "_L3OUT"
			},
			{
				"label": "L3OUT_SUBNET:",
				"name": "l3out_subnet"
			},
			{
				"label": "L3OUT_VLAN_ID:",
				"name": "l3out_vlan_id",
				"def": "0000"
			},
			{
				"label": "L3OUT_ACI_BLF1:",
				"name": "pod1_l3out_ip_aci_1"
			},
			{
				"label": "L3OUT_ACI_BLF2:",
				"name": "pod1_l3out_ip_aci_2"
			},
			{
				"label": "L3OUT_IP_FW:",
				"name": "l3out_ip_fw_1"
			},
			{
				"label": "BGP_ASN:",
				"name": "bgp_asn_aci",
				"def": "00000"
			},
			{
				"label": "DESCRIPTION:",
				"name": "description"
			}
		]
	} );

	var table = $('#ipplan').DataTable( {
		dom: 'Bfrtip',
		ajax: 'php/table.ipplan.php',
		searchPanes: {
			layout: "columns-4",
			columns: [0,1],
			cascadePanes: true,
			dataLength: 45, 
			dtOpts: {
                dom: "tp",
                paging: true,
                pagingType: 'numbers',
                searching: false,
				select:{
					style: 'multi'
				}
            } 
		},
		columns: [
			{
				"data": "tenant"
			},
			{
				"data": "vrf_name"
			},
			{
				"data": "vrf_aci_name"
			},
			{
				"data": "bd"
			},
			{
				"data": "subnet"
			},
			{
				"data": "anycast_gateway"
			},
			{
				"data": "appname"
			},
			{
				"data": "epg"
			},
			{
				"data": "vlan_id"
			},
			{
				"data": "vlan_name"
			},
			{
				"data": "fw_zone"
			},
			{
				"data": "l3out_name"
			},
			{
				"data": "l3out_subnet"
			},
			{
				"data": "l3out_vlan_id"
			},
			{
				"data": "pod1_l3out_ip_aci_1"
			},
			{
				"data": "pod1_l3out_ip_aci_2"
			},
			{
				"data": "l3out_ip_fw_1"
			},
			{
				"data": "bgp_asn_aci"
			},
			{
				"data": "description"
			}
		],
		columnDefs:[
			{
				searchPanes:{
					show:true,
					threshold: 1
				},
				targets: [0, 1, 3, 6, 7],
			}
		],
		select: 'single',
		lengthChange: true,
		scrollCollapse: true,
		scrollY: "800px",
        scrollX: true,
		pageLength: 15,
		//autoWidth: true,
		buttons: [
			{ extend: 'create', editor: editor },
			{ extend: 'edit',   editor: editor },
			{ extend: 'remove', editor: editor },
			{ extend: 'searchPanes', config: {cascadePanes: true}},
			{ extend: 'searchBuilder', config: {depthLimit: 3}},
			{ extend: 'excelHtml5' , text: 'Export Excel', sheetName: 'IPPLAN', filename: 'SAQ_IP_PLAN'}
		]
	} );
	$('#data-table').DataTable().searchPanes.rebuildPane();
} );

}(jQuery));

