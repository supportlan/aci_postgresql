<?php

	// DataTables PHP library and database connection
include( "lib/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;


// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'ipplan', 'id' )
	->fields(
		Field::inst( 'id' )
			->set( false )
			->validator( Validate::notEmpty() ),
		Field::inst( 'tenant' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'vrf_name' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'vrf_aci_name' )
			->set( true )
			->validator( Validate::notEmpty( ValidateOptions::inst() 
				->message( 'Correspond au nom de la VRF avec suffixe _VRF' ))),
		Field::inst( 'bd' )
			->set( true )
			->validator( Validate::notEmpty() ),
		Field::inst( 'subnet' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'anycast_gateway' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'appname' )
			->set( true ),
		Field::inst( 'epg' )
			->set( true ),
		Field::inst( 'vlan_id' )
			->validator( Validate::notEmpty() )
			->validator( Validate::numeric() ),
		Field::inst( 'vlan_name' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'fw_zone' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'l3out_name' )
			->set( true ),
		Field::inst( 'l3out_subnet' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'l3out_vlan_id' )
			->validator( Validate::notEmpty() )
			->validator( Validate::numeric() ),
		Field::inst( 'pod1_l3out_ip_aci_1' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'pod1_l3out_ip_aci_2' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'l3out_ip_fw_1' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'bgp_asn_aci' )
			->validator( Validate::notEmpty() )
			->validator( Validate::numeric() ),
		Field::inst( 'description' )
			->validator( Validate::notEmpty() )
	)
	->process( $_POST )
	->json();
