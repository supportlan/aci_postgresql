-- 
-- Editor SQL for DB table ipplan
-- Created by http://editor.datatables.net/generator
-- 

CREATE TABLE IF NOT EXISTS ipplan (
	id serial,
	tenant varchar(255),
	vrf_name varchar(255),
	vrf_aci_name varchar(255),
	bd varchar(255),
	subnet varchar(255),
	anycast_gateway varchar(255),
	appname varchar(255),
	epg varchar(255),
	vlan_id numeric(9,2),
	vlan_name varchar(255),
	fw_zone varchar(255),
	l3out_name varchar(255),
	l3out_subnet varchar(255),
	l3out_vlan_id numeric(9,2),
	pod1_l3out_ip_aci_1 varchar(255),
	pod1_l3out_ip_aci_2 varchar(255),
	l3out_ip_fw_1 varchar(255),
	bgp_asn_aci numeric(9,2),
	description varchar(255),
	PRIMARY KEY( id )
);