<?php

	// DataTables PHP library and database connection
include( "lib/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;


// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'porttable', 'id' )
	->fields(
		Field::inst( 'id' )
			->set( false )
			->validator( Validate::notEmpty() ),
		Field::inst( 'pod' )
			->validator( Validate::notEmpty() )
			->validator( Validate::numeric() ),
		Field::inst( 'speed' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'device' )
			->set( true )
			->validator( Validate::notEmpty()),
		Field::inst( 'mode_po' )
			->set( true ),
		Field::inst( 'comments' )
			->set( true ),
		Field::inst( 'vlan' )
			->validator( Validate::notEmpty() )
			->validator( Validate::numeric() ),
		Field::inst( 'trunk_vlan' )
			->validator( Validate::numeric() ),
		Field::inst( 'leaf' )
			->validator( Validate::notEmpty() )
			->validator( Validate::numeric() ),
		Field::inst( 'port' )
			->validator( Validate::notEmpty() )
			->validator( Validate::numeric() ),
		Field::inst( 'leaf_vpc' )
			->set( true ),
		Field::inst( 'type_po' )
			->validator( Validate::notEmpty() )
	)
	->process( $_POST )
	->json();
