
/*
 * Editor client script for DB table porttable
 * Created by http://editor.datatables.net/generator
 */

(function($){

$(document).ready(function() {
	var editor = new $.fn.dataTable.Editor( {
		ajax: 'php/table.porttable.php',
		table: '#porttable',
		fields: [
			{
                "label": "POD:",
                "name": "pod",
				"def": 1
            },
			{
                "label": "SPEED:",
                "name": "speed",
				"type": "select",
				"options": [
					"100M",
					"1G",
					"10G",
					"25G",
					"40G",
					"100G",
					"200G",
					"400G"
				]
            },
			{
                "label": "DEVICE:",
                "name": "device"
            },
			{
                "label": "MODE_PO:",
                "name": "mode_po",
				"type": "select",
				"options": [
					"",
					"LACP",
					"STATIC"
				]
            },
			{
                "label": "COMMENTS:",
                "name": "comments"
            },
			{
                "label": "VLAN:",
                "name": "vlan",
				"def": 1
            },
			{
                "label": "TRUNK_VLAN:",
                "name": "trunk_vlan"
            },
			{
                "label": "LEAF:",
                "name": "leaf"
            },
			{
                "label": "PORT:",
                "name": "port"
            },
			{
                "label": "LEAF_VPC:",
                "name": "leaf_vpc",
				"labelInfo": "Inscrire les 2 leafs ID formant<br>le VPC séparé par -,<br>leaf1ID-leaf2ID",
            },
			{
                "label": "TYPE_PO:",
                "name": "type_po",
				"type": "select",
				"options": [
					"SP",
					"PC",
					"VPC"
				]
            }
		]
	} );

	var table = $('#porttable').DataTable( {
		dom: 'Bfrtip',
		ajax: 'php/table.porttable.php',
		searchPanes: {
			layout: "columns-4",
			columns: [0,1],
			cascadePanes: true,
			dataLength: 45, 
			dtOpts: {
                dom: "tp",
                paging: true,
                pagingType: 'numbers',
                searching: false,
				select:{
					style: 'multi'
				}
            } 
		},
		columns: [
			{
				"data": "pod"
			},
			{
				"data": "speed"
			},
			{
				"data": "device"
			},
			{
				"data": "mode_po"
			},
			{
				"data": "comments"
			},
			{
				"data": "vlan"
			},
			{
				"data": "trunk_vlan"
			},
			{
				"data": "leaf"
			},
			{
				"data": "port"
			},
			{
				"data": "leaf_vpc"
			},
			{
				"data": "type_po"
			}
		],
		columnDefs:[
			{
				searchPanes:{
					show:true,
					threshold: 1
				},
				targets: [0, 1, 3, 6, 7],
			}
		],
		select: 'single',
		lengthChange: true,
		scrollCollapse: true,
		scrollY: "800px",
        scrollX: true,
		pageLength: 15,
		//autoWidth: true,
		buttons: [
			{ extend: 'create', editor: editor },
			{ extend: 'edit',   editor: editor },
			{ extend: 'remove', editor: editor },
			{ extend: 'searchPanes', config: {cascadePanes: true}},
			{ extend: 'searchBuilder', config: {depthLimit: 3}},
			{ extend: 'excelHtml5' , text: 'Export Excel', sheetName: 'PORTTABLE', filename: 'SAQ_PORT_TABLE'}
		]
	} );
	$('#data-table').DataTable().searchPanes.rebuildPane();
} );

}(jQuery));

