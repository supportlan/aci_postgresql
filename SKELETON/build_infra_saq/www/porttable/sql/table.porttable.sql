-- 
-- Editor SQL for DB table ipplan
-- Created by http://editor.datatables.net/generator
-- 

CREATE TABLE IF NOT EXISTS porttable
(
    id serial,
    pod smallint NOT NULL,
    speed character varying(50) COLLATE pg_catalog."default" NOT NULL,
    device character varying(50) COLLATE pg_catalog."default" NOT NULL,
    mode_po character varying(50) COLLATE pg_catalog."default",
    comments character varying(50) COLLATE pg_catalog."default" NOT NULL,
    vlan smallint NOT NULL,
    trunk_vlan character varying(50) COLLATE pg_catalog."default",
    leaf smallint NOT NULL,
    port smallint NOT NULL,
    leaf_vpc character varying(50) COLLATE pg_catalog."default",
    type_po character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT porttable_pkey PRIMARY KEY (id)
);