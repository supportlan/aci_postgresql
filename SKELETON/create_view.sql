CREATE VIEW public.ipplan_tenant
 AS
 SELECT DISTINCT (tenant), id FROM ipplan order by tenant,id asc;

CREATE VIEW public.ipplan_tenant_unique
 AS
 SELECT DISTINCT (tenant) FROM ipplan order by tenant asc;

CREATE VIEW public.ipplan_appname
 AS
 SELECT DISTINCT (appname), tenant, id FROM ipplan order by tenant,id asc;

CREATE VIEW public.ipplan_vrf
 AS
 SELECT DISTINCT (vrf_aci_name), tenant, id FROM ipplan order by tenant,id asc;

CREATE VIEW public.ipplan_bridge_domain
 AS
 SELECT DISTINCT(bd), subnet, anycast_gateway, vrf_aci_name, tenant, vlan_id, id FROM ipplan order by tenant,id asc;

CREATE VIEW public.ipplan_subnet_gateway
 AS
 SELECT DISTINCT(bd), subnet, anycast_gateway, vrf_aci_name, tenant, vlan_id, concat(anycast_gateway, RIGHT(subnet,3)) as subnet_gw , id FROM ipplan where subnet != 'L2' order by tenant,id asc;

CREATE VIEW public.ipplan_epg
 AS
 SELECT DISTINCT epg, appname,tenant,id FROM ipplan order by tenant,id asc;

CREATE VIEW public.ipplan_l3outconfiguration_global
 AS
 SELECT DISTINCT vrf_name,vrf_aci_name,tenant,l3out_name,l3out_subnet,l3out_vlan_id,
    left(l3out_name,3) as siteid,concat(pod1_l3out_ip_aci_1,right(l3out_subnet,3)) as pod1_aci1,
    concat(pod1_l3out_ip_aci_2,right(l3out_subnet,3)) as pod1_aci2,l3out_ip_fw_1,bgp_asn_aci,id
 FROM ipplan where subnet != 'L2'
 order by tenant,id asc;

CREATE VIEW public.ipplan_bgp_per_vrf
 AS
 SELECT DISTINCT vrf_name,vrf_aci_name,bgp_asn_aci,l3out_name,tenant,
    left(l3out_name,3) as siteid,id 
 FROM ipplan where subnet != 'L2'
 order by tenant,id asc;

CREATE VIEW public.ipplan_phy_vlan_pool
 AS
 SELECT DISTINCT(vlan_id), description, id FROM ipplan order by id asc;

CREATE VIEW public.ipplan_l3_vlan_pool
 AS
 SELECT DISTINCT(l3out_vlan_id),l3out_name,id from ipplan where fw_zone is not null order by id asc;

CREATE VIEW public.ipplan_aaep
 AS
 SELECT DISTINCT(vlan_id), description, ID, 'phydom' as type_dom, 
    CONCAT(id,'_','phydom') as phy_l3o_dom 
 FROM ipplan UNION SELECT DISTINCT(l3out_vlan_id),l3out_name,id, 
    'l3dom' as type_dom, CONCAT(id,'_','l3dom') as phy_l3o_dom 
 FROM ipplan where fw_zone is not null;

CREATE VIEW public.porttable_vpc_groups
 AS
 select distinct(leaf_vpc), 
 DENSE_RANK() OVER (ORDER BY  leaf_vpc)  AS row_number from porttable where type_po = 'VPC'
 order by row_number;;

CREATE VIEW public.porttable_vpc_policy_groups
 AS
 SELECT DISTINCT(device),speed,mode_po,leaf_vpc,type_po,
    DENSE_RANK() OVER (ORDER BY  device)  AS row_number 
 FROM porttable where type_po = 'VPC'
 order by row_number;

CREATE VIEW public.porttable_pc_policy_groups
 AS
 SELECT DISTINCT(device),speed,mode_po,leaf_vpc,type_po,
    DENSE_RANK() OVER (ORDER BY  device)  AS row_number 
 FROM porttable where type_po = 'PC'
 order by row_number;

CREATE VIEW public.porttable_sp_profile
 AS
 SELECT DISTINCT(leaf) from porttable where type_po = 'SP';

CREATE VIEW public.porttable_pc_profile
 AS
 SELECT DISTINCT(leaf) from porttable where type_po = 'PC';

CREATE VIEW public.porttable_vpc_profile
 AS
 SELECT DISTINCT(leaf_vpc) FROM porttable where type_po = 'VPC';

CREATE VIEW public.porttable_switch_sp_pc_profile
 AS
 select distinct(leaf) from porttable where type_po = 'SP' and 
    leaf not in (select distinct(leaf) 
 from porttable where type_po = 'PC') 
 union select distinct(leaf) from porttable where type_po = 'PC';

CREATE VIEW public.porttable_switch_sp_pc_profile_detail
 AS
 select distinct(leaf),type_po from porttable where type_po = 'SP' 
 union select distinct(leaf),type_po from porttable where type_po = 'PC';

CREATE VIEW public.porttable_sp_port_block
 AS
 select distinct(leaf),device,speed,port,CONCAT(leaf,'_',port) as pkey from porttable where type_po = 'SP';

CREATE VIEW public.porttable_pc_port_block
 AS
 select distinct(leaf),device,speed,port,CONCAT(leaf,'_',port) as pkey from porttable where type_po = 'PC';

CREATE VIEW public.porttable_vpc_port_block
 AS
 select distinct(leaf_vpc),device,speed,port,CONCAT(leaf_vpc,'_',port) as pkey from porttable where type_po = 'VPC';

CREATE VIEW public.porttable_port_description
 AS
 select distinct id,leaf,port,device,comments,type_po,pod from porttable order by id;

CREATE VIEW public.porttable_attach_sp_access
 AS
 select distinct ipplan.tenant,ipplan.appname,ipplan.epg,
 left(cast(porttable.leaf as char),1) as podid,porttable.leaf,porttable.port,
 porttable.vlan,CONCAT(porttable.LEAF,'_',porttable.port,'_',porttable.vlan) as pkey 
 from ipplan inner join porttable 
 on ipplan.vlan_id = porttable.vlan 
 where porttable.type_po = 'SP' and porttable.trunk_vlan = '' 
 and porttable.vlan != '1';

CREATE VIEW public.porttable_attach_sp_native
 AS
 select distinct ipplan.tenant,ipplan.appname,ipplan.epg,
 left(CAST(porttable.leaf as CHAR),1) as podid,porttable.leaf,porttable.port,
 porttable.vlan,CONCAT(porttable.LEAF,'_',porttable.port,'_',porttable.vlan) as pkey 
 from ipplan inner join porttable 
 on ipplan.vlan_id = porttable.vlan 
 where porttable.type_po = 'SP' and porttable.trunk_vlan != '' 
 and porttable.vlan != '1';

CREATE VIEW public.porttable_attach_sp_trunk
 AS
 select distinct ipplan.tenant,ipplan.appname,ipplan.epg,
 left(cast(porttable.leaf as char),1) as podid,porttable.leaf,porttable.port,
 porttable.trunk_vlan,CONCAT(porttable.LEAF,'_',porttable.port,'_',porttable.trunk_vlan) as pkey 
 from ipplan inner join porttable 
 on ipplan.vlan_id::text = porttable.trunk_vlan
 where porttable.type_po = 'SP';

CREATE VIEW public.porttable_attach_pc_access
 AS
 select distinct ipplan.tenant,ipplan.appname,ipplan.epg,
 left(cast(porttable.leaf as char),1) as podid,porttable.leaf_vpc,porttable.device,
 porttable.port,porttable.vlan,CONCAT(porttable.LEAF,'_',porttable.port,'_',porttable.vlan) as pkey 
 from ipplan inner join porttable 
 on ipplan.vlan_id = porttable.vlan 
 where porttable.type_po = 'PC' and porttable.trunk_vlan = '' and porttable.vlan != '1';

CREATE VIEW public.porttable_attach_pc_native
 AS
 select distinct ipplan.tenant,ipplan.appname,ipplan.epg,
 left(cast(porttable.leaf as char),1) as podid,porttable.leaf_vpc,porttable.device,
 porttable.port,porttable.vlan,CONCAT(porttable.LEAF,'_',porttable.port,'_',porttable.vlan) as pkey 
 from ipplan inner join porttable 
 on ipplan.vlan_id = porttable.vlan 
 where porttable.type_po = 'PC' and porttable.trunk_vlan != '' and porttable.vlan != '1';


CREATE VIEW public.porttable_attach_pc_trunk
 AS
 select distinct ipplan.tenant,ipplan.appname,ipplan.epg,
 left(cast(porttable.leaf as char),1) as podid,porttable.leaf_vpc,porttable.device,
 porttable.port,porttable.trunk_vlan,CONCAT(porttable.LEAF,'_',porttable.port,'_',porttable.trunk_vlan) as pkey 
 from ipplan inner join porttable 
 on ipplan.vlan_id::text = porttable.trunk_vlan 
 where porttable.type_po = 'PC';

CREATE VIEW public.porttable_attach_vpc_access
 AS
 select distinct ipplan.tenant,ipplan.appname,ipplan.epg,
 left(cast(porttable.leaf as char),1) as podid,porttable.leaf_vpc,porttable.device,
 porttable.port,porttable.vlan,CONCAT(porttable.LEAF,'_',porttable.port,'_',porttable.vlan) as pkey 
 from ipplan inner join porttable 
 on ipplan.vlan_id = porttable.vlan 
 where porttable.type_po = 'VPC' and porttable.trunk_vlan = '' and porttable.vlan != '1';

CREATE VIEW public.porttable_attach_vpc_native
 AS
 select distinct ipplan.tenant,ipplan.appname,ipplan.epg,
 left(cast(porttable.leaf as char),1) as podid,porttable.leaf_vpc,porttable.device,
 porttable.port,porttable.vlan,CONCAT(porttable.LEAF,'_',porttable.port,'_',porttable.vlan) as pkey 
 from ipplan inner join porttable 
 on ipplan.vlan_id = porttable.vlan 
 where porttable.type_po = 'VPC' and porttable.trunk_vlan != '' and porttable.vlan != '1';


CREATE VIEW public.porttable_attach_vpc_trunk
 AS
 select distinct ipplan.tenant,ipplan.appname,ipplan.epg,
 left(cast(porttable.leaf as char),1) as podid,porttable.leaf_vpc,porttable.device,
 porttable.port,porttable.trunk_vlan,CONCAT(porttable.LEAF,'_',porttable.port,'_',porttable.trunk_vlan) as pkey 
 from ipplan inner join porttable 
 on ipplan.vlan_id::text = porttable.trunk_vlan 
 where porttable.type_po = 'VPC';


