-- Table: public.porttable

-- DROP TABLE IF EXISTS public.porttable;

CREATE TABLE porttable (
	id serial NOT NULL,
	pod smallint NOT NULL,
	speed varchar(50) NOT NULL,
	device varchar(50) NOT NULL,
	mode_po varchar(50),
	"comments" varchar(50) NOT NULL,
	vlan smallint NOT NULL,
	trunk_vlan varchar(50),
	leaf smallint NOT NULL,
	port smallint NOT NULL,
	leaf_vpc varchar(50),
	type_po varchar(50) NOT NULL,
	PRIMARY KEY (id)
);
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (1, 1, '40G', 'SMY_DC_FW01', 'LACP', 'VPC Palo Alto Internal FW', 1, '2764', 1431, 51, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (2, 1, '40G', 'SMY_DC_FW01', 'LACP', 'VPC Palo Alto Internal FW', 1, '3799', 1431, 51, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (3, 1, '40G', 'SMY_DC_FW01', 'LACP', 'VPC Palo Alto Internal FW', 1, '3772', 1431, 51, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (4, 1, '40G', 'SMY_DC_FW01', 'LACP', 'VPC Palo Alto Internal FW', 1, '2797', 1431, 51, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (5, 1, '40G', 'SMY_DC_FW02', 'LACP', 'VPC Palo Alto Internal FW', 1, '2764', 1431, 52, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (6, 1, '40G', 'SMY_DC_FW02', 'LACP', 'VPC Palo Alto Internal FW', 1, '3799', 1431, 52, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (7, 1, '40G', 'SMY_DC_FW02', 'LACP', 'VPC Palo Alto Internal FW', 1, '3772', 1431, 52, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (8, 1, '40G', 'SMY_DC_FW02', 'LACP', 'VPC Palo Alto Internal FW', 1, '2797', 1431, 52, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (9, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '2716', 1431, 3, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (10, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '2717', 1431, 3, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (11, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 1431, 3, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (12, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 1431, 3, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (13, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 1431, 3, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (14, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 1431, 3, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (15, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '2716', 1431, 4, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (16, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '2717', 1431, 4, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (17, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 1431, 4, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (18, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 1431, 4, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (19, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 1431, 4, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (20, 1, '10G', 'SMY_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 1431, 4, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (21, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '2716', 1431, 5, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (22, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '2717', 1431, 5, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (23, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 1431, 5, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (24, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 1431, 5, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (25, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 1431, 5, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (26, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 1431, 5, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (27, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '2716', 1431, 6, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (28, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '2717', 1431, 6, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (29, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 1431, 6, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (30, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 1431, 6, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (31, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 1431, 6, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (32, 1, '10G', 'SMY_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 1431, 6, '1431-1441', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (33, 1, '40G', 'RIN_DC_FW01', 'LACP', 'VPC Palo Alto Internal FW', 1, '2764', 2231, 51, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (34, 1, '40G', 'RIN_DC_FW01', 'LACP', 'VPC Palo Alto Internal FW', 1, '3799', 2231, 51, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (35, 1, '40G', 'RIN_DC_FW01', 'LACP', 'VPC Palo Alto Internal FW', 1, '3772', 2231, 51, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (36, 1, '40G', 'RIN_DC_FW01', 'LACP', 'VPC Palo Alto Internal FW', 1, '2797', 2231, 51, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (37, 1, '40G', 'RIN_DC_FW02', 'LACP', 'VPC Palo Alto Internal FW', 1, '2764', 2231, 52, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (38, 1, '40G', 'RIN_DC_FW02', 'LACP', 'VPC Palo Alto Internal FW', 1, '3799', 2231, 52, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (39, 1, '40G', 'RIN_DC_FW02', 'LACP', 'VPC Palo Alto Internal FW', 1, '3772', 2231, 52, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (40, 1, '40G', 'RIN_DC_FW02', 'LACP', 'VPC Palo Alto Internal FW', 1, '2797', 2231, 52, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (41, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '2716', 2231, 3, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (42, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '2717', 2231, 3, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (43, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 2231, 3, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (44, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 2231, 3, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (45, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 2231, 3, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (46, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 2231, 3, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (47, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '2716', 2231, 4, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (48, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '2717', 2231, 4, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (49, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 2231, 4, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (50, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 2231, 4, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (51, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 2231, 4, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (52, 1, '10G', 'RIN_EDGE_FW03', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 2231, 4, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (53, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '2716', 2231, 5, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (54, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '2717', 2231, 5, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (55, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 2231, 5, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (56, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 2231, 5, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (57, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 2231, 5, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (58, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 2231, 5, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (59, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '2716', 2231, 6, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (60, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '2717', 2231, 6, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (61, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 2231, 6, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (62, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '1307', 2231, 6, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (63, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 2231, 6, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (64, 1, '10G', 'RIN_EDGE_FW04', 'LACP', 'VPC Palo Alto External FW', 1, '3717', 2231, 6, '2231-2241', 'VPC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (65, 1, '10G', 'SMY_LB03_EXT_HA_P3', null, 'F5 LB External HA', 1012, null, 1431, 11, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (66, 1, '10G', 'SMY_LB03_EXT_HA_P4', null, 'F5 LB External HA', 1012, null, 1441, 11, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (67, 1, '10G', 'SMY_LB04_EXT_HA_P3', null, 'F5 LB External HA', 1012, null, 1431, 12, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (68, 1, '10G', 'SMY_LB04_EXT_HA_P4', null, 'F5 LB External HA', 1012, null, 1441, 12, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (69, 1, '10G', 'RIN_LB03_EXT_HA_P3', null, 'F5 LB External HA', 1112, null, 2231, 11, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (70, 1, '10G', 'RIN_LB03_EXT_HA_P4', null, 'F5 LB External HA', 1112, null, 2241, 11, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (71, 1, '10G', 'RIN_LB04_EXT_HA_P3', null, 'F5 LB External HA', 1112, null, 2231, 12, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (72, 1, '10G', 'RIN_LB04_EXT_HA_P4', null, 'F5 LB External HA', 1112, null, 2241, 12, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (73, 1, '10G', 'CENTURY-RTR-1', null, 'CENTURY-RTR-1', 3061, null, 2231, 48, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (74, 1, '10G', 'ATT-RTR-2', null, 'ATT-RTR-2', 3062, null, 2241, 48, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (75, 1, '10G', 'DC-US-RIN-0002-R203-RT-01', 'LACP', 'DC-US-RIN-0002-R203-RT-01', 3061, null, 2231, 1, '2231', 'PC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (76, 1, '10G', 'DC-US-RIN-0002-R203-RT-01', 'LACP', 'DC-US-RIN-0002-R203-RT-01', 3061, null, 2231, 2, '2231', 'PC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (77, 1, '10G', 'DC-US-RIN-0002-R204-RT-02', 'LACP', 'DC-US-RIN-0002-R204-RT-02', 3062, null, 2241, 1, '2241', 'PC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (78, 1, '10G', 'DC-US-RIN-0002-R204-RT-02', 'LACP', 'DC-US-RIN-0002-R204-RT-02', 3062, null, 2241, 2, '2241', 'PC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (79, 1, '10G', 'CENTURY-RTR-1', null, 'CENTURY-RTR-1', 3051, null, 1431, 48, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (80, 1, '10G', 'ATT-RTR-2', null, 'ATT-RTR-2', 3052, null, 1441, 48, null, 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (81, 1, '10G', 'DC-US-SMY-0001-CR50-RT-01', 'LACP', 'DC-US-SMY-0001-CR50-RT-01', 3051, null, 1431, 1, '1431', 'PC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (82, 1, '10G', 'DC-US-SMY-0001-CR50-RT-01', 'LACP', 'DC-US-SMY-0001-CR50-RT-01', 3051, null, 1431, 2, '1431', 'PC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (83, 1, '10G', 'DC-US-SMY-0001-CR51-RT-02', 'LACP', 'DC-US-SMY-0001-CR51-RT-02', 3052, null, 1441, 1, '1441', 'PC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (84, 1, '10G', 'DC-US-SMY-0001-CR51-RT-02', 'LACP', 'DC-US-SMY-0001-CR51-RT-02', 3052, null, 1441, 2, '1441', 'PC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (85, 1, '100M', 'TEST1234', '', 'TEST1234', 1, '222', 2222, 11, '', 'SP');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (86, 1, '25G', 'TEST1234-1', 'LACP', 'TEST1234-1', 122, '', 2222, 33, '2222', 'PC');
INSERT INTO porttable(id, pod, speed, device, mode_po, "comments", vlan, trunk_vlan, leaf, port, leaf_vpc, type_po) VALUES (87, 1, '100G', 'TEST1234-2', 'STATIC', 'TEST1234-2', 1, '3333', 2222, 14, '2222-2224', 'VPC');
