variable "username" {
	default = "adm_api"
}

variable "cert_name" {
	default = "adm_api"
}

variable "private_key"{
	default = "./adm_api.key"
}

variable "url" {
	default = "https://192.168.10.1"
}

variable "company_name" {
	default = "SAQ"
}

variable "bgp_holdtime" {
	default = "17"
}

variable "bgp_keepalive" {
	default = "5"
}

variable "vlan_pool" {
	type = map
	default = {
		vlan_pool_phy_dom = {
			name   = "SAQ_PHY_STAT_VLP",
			type   = "static",
			desc   = "NAM Vlan Pool for Physical Domain",
			phydom = "SERVERS_PHY_DOM"
		},
		vlan_pool_l3_dom = {
			name   = "SAQ_L3OUT_STAT_VLP",
			type   = "static",
			desc   = "NAM Vlan Pool for L3OUT Domain",
			phydom = "L3DCI_L3O_DOM"
		}
	}
}

variable "lldp_profile" {
	type = map
	default = {
		lldp_disable = {
			status   = "DISABLE",
			receive  = "disabled",
			transmit = "disabled"
		},
		lldp_enable = {
			status   = "ENABLE",
			receive  = "enabled",
			transmit = "enabled"
		}
	}
}

variable "cdp_profile" {
	type = map
	default = {
		cdp_disable = {
			status   = "DISABLE",
			state    = "disabled"
		},
		cdp_enable = {
			status   = "ENABLE",
			state    = "enabled"
		}
	}
}

variable "link_level_profile" {
	type = map
	default = {
		link100m = {
			link     = "100",
			speed    = "100M"
		},
		link1g = {
			link     = "1000",
			speed    = "1G"
		},
		link10g = {
			link     = "10000",
			speed    = "10G"
		},
		link25g = {
			link     = "25000",
			speed    = "25G"
		},
		link40g = {
			link     = "40000",
			speed    = "40G"
		},
		link50g = {
			link     = "50000",
			speed    = "50G"
		},
		link100g = {
			link     = "100000",
			speed    = "100G"
		},
		link200g = {
			link     = "200000",
			speed    = "200G"
		},
		link400g = {
			link     = "400000",
			speed    = "400G"
		}
	}
}

variable "port_channel_profile" {
	type = map
	default = {
		po_lacp = {
			desc     = "LACP port-channel policy",
			mode     = "active",
			type     = "LACP_ACTIVE"			
		},
		po_static = {
			desc     = "Mode ON port-channel policy",
			mode     = "off",
			type     = "STATIC_ON"
		},
		po_mac_pin = {
			desc     = "Mac Pinning port-channel policy",
			mode     = "mac-pin",
			type     = "MAC_PINNING"
		},
	}
}

variable "single_port_policy_group" {
	type = map
	default = {
		link100mdisable = {
			speed    = "100MBPS",
			cdp		 = "cdp_disable",
			lldp     = "lldp_disable",
			discovery = "DISABLE",
			linklevel = "link100m"
		},
		link100menable = {
			speed    = "100MBPS",
			cdp		 = "cdp_enable",
			lldp     = "lldp_enable",
			discovery = "ENABLE",
			linklevel = "link100m"
		},
		link1gdisable = {
			speed    = "1GBPS",
			cdp		 = "cdp_disable",
			lldp     = "lldp_disable",
			discovery = "DISABLE",
			linklevel = "link1g"
		},
		link1genable = {
			speed    = "1GBPS",
			cdp		 = "cdp_enable",
			lldp     = "lldp_enable",
			discovery = "ENABLE",
			linklevel = "link1g"
		},
		link10gdisable = {
			speed    = "10GBPS",
			cdp		 = "cdp_disable",
			lldp     = "lldp_disable",
			discovery = "DISABLE",
			linklevel = "link10g"
		},
		link10genable = {
			speed    = "10GBPS",
			cdp		 = "cdp_enable",
			lldp     = "lldp_enable",
			discovery = "ENABLE",
			linklevel = "link10g"
		},
		link25gdisable = {
			speed    = "25GBPS",
			cdp		 = "cdp_disable",
			lldp     = "lldp_disable",
			discovery = "DISABLE",
			linklevel = "link25g"
		},
		link25genable = {
			speed    = "25GBPS",
			cdp		 = "cdp_enable",
			lldp     = "lldp_enable",
			discovery = "ENABLE",
			linklevel = "link25g"
		},
		link40gdisable = {
			speed    = "40GBPS",
			cdp		 = "cdp_disable",
			lldp     = "lldp_disable",
			discovery = "DISABLE",
			linklevel = "link40g"
		},
		link40genable = {
			speed    = "40GBPS",
			cdp		 = "cdp_enable",
			lldp     = "lldp_enable",
			discovery = "ENABLE",
			linklevel = "link40g"
		},
		link50gdisable = {
			speed    = "1GBPS",
			cdp		 = "cdp_disable",
			lldp     = "lldp_disable",
			discovery = "DISABLE",
			linklevel = "link50g"
		},
		link50genable = {
			speed    = "50GBPS",
			cdp		 = "cdp_enable",
			lldp     = "lldp_enable",
			discovery = "ENABLE",
			linklevel = "link50g"
		},
		link100gdisable = {
			speed    = "100GBPS",
			cdp		 = "cdp_disable",
			lldp     = "lldp_disable",
			discovery = "DISABLE",
			linklevel = "link100g"
		},
		link100genable = {
			speed    = "100GBPS",
			cdp		 = "cdp_enable",
			lldp     = "lldp_enable",
			discovery = "ENABLE",
			linklevel = "link100g"
		},
		link200gdisable = {
			speed    = "200GBPS",
			cdp		 = "cdp_disable",
			lldp     = "lldp_disable",
			discovery = "DISABLE",
			linklevel = "link200g"
		},
		link200genable = {
			speed    = "200GBPS",
			cdp		 = "cdp_enable",
			lldp     = "lldp_enable",
			discovery = "ENABLE",
			linklevel = "link200g"
		},
		link400gdisable = {
			speed    = "400GBPS",
			cdp		 = "cdp_disable",
			lldp     = "lldp_disable",
			discovery = "DISABLE",
			linklevel = "link400g"
		},
		link400genable = {
			speed    = "400GBPS",
			cdp		 = "cdp_enable",
			lldp     = "lldp_enable",
			discovery = "ENABLE",
			linklevel = "link400g"
		}
	}
}

