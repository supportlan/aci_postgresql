# variable "target_group_depends_on_1" {
#   type    = any
#   default = null
# }

locals {
  local_data_ipplan_phy_vlan_pool = jsondecode(file("${path.root}/ipplan_phy_vlan_pool.json"))
  local_data_ipplan_l3_vlan_pool = jsondecode(file("${path.root}/ipplan_l3_vlan_pool.json"))
  local_data_ipplan_aaep = jsondecode(file("${path.root}/ipplan_aaep.json"))
  local_data_porttable_vpc_groups = jsondecode(file("${path.root}/porttable_vpc_groups.json"))
  local_data_porttable_vpc_policy_groups = jsondecode(file("${path.root}/porttable_vpc_policy_groups.json"))
  local_data_porttable_pc_policy_groups = jsondecode(file("${path.root}/porttable_pc_policy_groups.json"))
  local_data_porttable_sp_profile = jsondecode(file("${path.root}/porttable_sp_profile.json"))
  local_data_porttable_pc_profile = jsondecode(file("${path.root}/porttable_pc_profile.json"))
  local_data_porttable_vpc_profile = jsondecode(file("${path.root}/porttable_vpc_profile.json"))
  local_data_porttable_switch_sp_pc_profile = jsondecode(file("${path.root}/porttable_switch_sp_pc_profile.json"))
  local_data_porttable_switch_sp_pc_profile_detail = jsondecode(file("${path.root}/porttable_switch_sp_pc_profile_detail.json"))
  local_data_porttable_sp_port_block = jsondecode(file("${path.root}/porttable_sp_port_block.json"))
  local_data_porttable_pc_port_block = jsondecode(file("${path.root}/porttable_pc_port_block.json"))
  local_data_porttable_vpc_port_block = jsondecode(file("${path.root}/porttable_vpc_port_block.json"))
  local_data_porttable_port_description = jsondecode(file("${path.root}/porttable_port_description.json"))
}

resource "aci_vlan_pool" "configure_vlan_pool" { 
  for_each    = var.vlan_pool
  name        = each.value.name
  alloc_mode  = each.value.type
  description = each.value.desc
}

resource "aci_ranges" "servers_stat_vlp_vlanpool" {
  for_each      = {for vlan_pool in local.local_data_ipplan_phy_vlan_pool : vlan_pool.id => vlan_pool}
  vlan_pool_dn  = aci_vlan_pool.configure_vlan_pool["vlan_pool_phy_dom"].id
  to            = "vlan-${each.value.vlan_id}"
  from          = "vlan-${each.value.vlan_id}"
  alloc_mode    = aci_vlan_pool.configure_vlan_pool["vlan_pool_phy_dom"].alloc_mode
  description   = "${lower(each.value.description)}"
}

resource "aci_ranges" "l3_stat_vlp_vlanpool" {
  for_each      = {for vlan_pool in local.local_data_ipplan_l3_vlan_pool : vlan_pool.id => vlan_pool}
  vlan_pool_dn  = aci_vlan_pool.configure_vlan_pool["vlan_pool_l3_dom"].id
  to            = "vlan-${each.value.l3out_vlan_id}"
  from          = "vlan-${each.value.l3out_vlan_id}"
  alloc_mode    = aci_vlan_pool.configure_vlan_pool["vlan_pool_l3_dom"].alloc_mode
  description   = "${lower(each.value.l3out_name)}"
}

resource "aci_physical_domain" "create_phy_dom" {
  for_each                  = {for vlan_pool in local.local_data_ipplan_phy_vlan_pool : vlan_pool.id => vlan_pool}
  name                      = var.vlan_pool["vlan_pool_phy_dom"].phydom
  relation_infra_rs_vlan_ns = aci_ranges.servers_stat_vlp_vlanpool[each.value.id].vlan_pool_dn
}

resource "aci_l3_domain_profile" "create_l3_dom" {
  for_each                    = {for vlan_pool in local.local_data_ipplan_l3_vlan_pool : vlan_pool.id => vlan_pool}
  name                        = var.vlan_pool["vlan_pool_l3_dom"].phydom
  relation_infra_rs_vlan_ns   = aci_ranges.l3_stat_vlp_vlanpool[each.value.id].vlan_pool_dn
}

resource "aci_attachable_access_entity_profile" "create_aaep_phy_dom" {
  for_each                = {for aaep_row in local.local_data_ipplan_aaep : aaep_row.phy_l3o_dom => aaep_row}
  description             = "${var.company_name} AAEP"
  name                    = "${var.company_name}_PHY_L3O_DOM_AAEP"
  #relation_infra_rs_dom_p = [each.value.type_dom == "phydom" ? aci_physical_domain.create_phy_dom[each.value.pkey].id : aci_l3_domain_profile.create_l3_dom[each.value.pkey].id]
  relation_infra_rs_dom_p = ["uni/phys-${var.vlan_pool["vlan_pool_phy_dom"].phydom}","uni/l3dom-${var.vlan_pool["vlan_pool_l3_dom"].phydom}"]
}

resource "aci_lldp_interface_policy" "create_lldp_policies" {
  for_each    = var.lldp_profile
  description = "Policy for LLDP in ${each.value.status} state"
  name        = "LLDP_${upper(each.value.status)}_INTPOL"
  admin_rx_st = each.value.receive
  admin_tx_st = each.value.transmit
} 

resource "aci_cdp_interface_policy" "create_cdp_policies" {
  for_each    = var.cdp_profile
  description = "Policy for CDP in ${each.value.status} state"
  name        = "CDP_${upper(each.value.status)}_INTPOL"
  admin_st    = each.value.state
} 

resource "aci_rest" "create_link_level_auto" {
  for_each    = var.link_level_profile
  path       = "/api/node/mo/uni/infra/hintfpol-${each.value.speed}BPS_AUTO_INTPOL.json"
  class_name = "fabricHIfPol"
  content = {
    "dn" = "uni/infra/hintfpol-${each.value.speed}BPS_AUTO_INTPOL"
    "name" = "${each.value.speed}BPS_AUTO_INTPOL"
    "autoNeg" = "on"
    "speed" = "${each.value.speed}"
    "rn" = "hintfpol-${each.value.speed}BPS_AUTO_INTPOL"
  }
}

resource "aci_rest" "create_link_level_full" {
  for_each    = var.link_level_profile
  path       = "/api/node/mo/uni/infra/hintfpol-${each.value.speed}BPS_FULL_INTPOL.json"
  class_name = "fabricHIfPol"
  content = {
    "dn" = "uni/infra/hintfpol-${each.value.speed}BPS_FULL_INTPOL"
    "name" = "${each.value.speed}BPS_FULL_INTPOL"
    "autoNeg" = "off"
    "speed" = "${each.value.speed}"
    "rn" = "hintfpol-${each.value.speed}BPS_FULL_INTPOL"
  }
}

resource "aci_rest" "create_po_policy" {
  for_each    = var.port_channel_profile
  path       = "/api/node/mo/uni/infra/lacplagp-PO_${each.value.type}_INTPOL.json"
  class_name = "lacpLagPol"
  content = {
    "dn" = "uni/infra/lacplagp-PO_${each.value.type}_INTPOL"
    "name" = "PO_${each.value.type}_INTPOL"
    "rn" = "lacplagp-PO_${each.value.type}_INTPOL"
    "mode" = each.value.mode
  }
}

resource "aci_vpc_explicit_protection_group" "create_vpc_groups" {
  for_each                          = {for vpc_group_row in local.local_data_porttable_vpc_groups : vpc_group_row.row_number => vpc_group_row}
  name                              = format("L%s_L%s_VPCG",split("-",each.value.leaf_vpc)[0],split("-",each.value.leaf_vpc)[1])
  switch1                           = split("-",each.value.leaf_vpc)[0]
  switch2                           = split("-",each.value.leaf_vpc)[1]
  vpc_domain_policy                 = "default"
  vpc_explicit_protection_group_id  = each.value.row_number
}

resource "aci_leaf_access_port_policy_group" "create_standard_port_auto" {
    for_each    = var.single_port_policy_group
    description = "Single Port Policy Group ${each.value.speed} Neighbor protocole ${each.value.discovery}"
    name        = "SP_${each.value.speed}_AUTO_${each.value.discovery}_IPG"
    relation_infra_rs_lldp_if_pol = aci_lldp_interface_policy.create_lldp_policies["${each.value.lldp}"].id
    relation_infra_rs_h_if_pol = aci_rest.create_link_level_auto[each.value.linklevel].id
    relation_infra_rs_cdp_if_pol = aci_cdp_interface_policy.create_cdp_policies["${each.value.cdp}"].id
    relation_infra_rs_att_ent_p = "uni/infra/attentp-${var.company_name}_PHY_L3O_DOM_AAEP"
}

resource "aci_leaf_access_port_policy_group" "create_standard_port_full" {
    for_each    = var.single_port_policy_group
    description = "Single Port Policy Group ${each.value.speed} Neighbor protocole ${each.value.discovery}"
    name        = "SP_${each.value.speed}_FULL_${each.value.discovery}_IPG"
    relation_infra_rs_lldp_if_pol = aci_lldp_interface_policy.create_lldp_policies["${each.value.lldp}"].id
    relation_infra_rs_h_if_pol = aci_rest.create_link_level_full[each.value.linklevel].id
    relation_infra_rs_cdp_if_pol = aci_cdp_interface_policy.create_cdp_policies["${each.value.cdp}"].id
    relation_infra_rs_att_ent_p = "uni/infra/attentp-${var.company_name}_PHY_L3O_DOM_AAEP"
}

resource "aci_leaf_access_bundle_policy_group" "create_vpc_policy_groups" {
  for_each                        = {for vpc_policy_group_row in local.local_data_porttable_vpc_policy_groups : vpc_policy_group_row.row_number => vpc_policy_group_row}
  name                            = "VPC_${upper(replace(each.value.device," ","_"))}_IPG"
  description                     = "VPC for device ${upper(replace(each.value.device," ","_"))}"
  lag_t                           = "node"
  relation_infra_rs_cdp_if_pol    = aci_cdp_interface_policy.create_cdp_policies["cdp_disable"].id
  relation_infra_rs_lldp_if_pol   = aci_lldp_interface_policy.create_lldp_policies["lldp_disable"].id
  relation_infra_rs_att_ent_p     = "uni/infra/attentp-${var.company_name}_PHY_L3O_DOM_AAEP"
  relation_infra_rs_h_if_pol      = aci_rest.create_link_level_auto["link${lower(each.value.speed)}"].id
  relation_infra_rs_lacp_pol      = aci_rest.create_po_policy["po_${lower(each.value.mode_po)}"].id
}

resource "aci_leaf_access_bundle_policy_group" "create_pc_policy_groups" {
  for_each                        = {for pc_policy_group_row in local.local_data_porttable_pc_policy_groups : pc_policy_group_row.row_number => pc_policy_group_row}
  name                            = "PC_${upper(replace(each.value.device," ","_"))}_IPG"
  description                     = "PC for device ${upper(replace(each.value.device," ","_"))}"
  lag_t                           = "link"
  relation_infra_rs_cdp_if_pol    = aci_cdp_interface_policy.create_cdp_policies["cdp_disable"].id
  relation_infra_rs_lldp_if_pol   = aci_lldp_interface_policy.create_lldp_policies["lldp_disable"].id
  relation_infra_rs_att_ent_p     = "uni/infra/attentp-${var.company_name}_PHY_L3O_DOM_AAEP"
  relation_infra_rs_h_if_pol      = aci_rest.create_link_level_auto["link${lower(each.value.speed)}"].id
  relation_infra_rs_lacp_pol      = aci_rest.create_po_policy["po_${lower(each.value.mode_po)}"].id
}

resource "aci_leaf_interface_profile" "create_single_leaf_profile" {
  for_each = {for sp_profile_row in local.local_data_porttable_sp_profile : sp_profile_row.leaf => sp_profile_row}
  name        = "L${each.value.leaf}_IPR"
  description  = "Interface Profile Single Port for Leaf L${each.value.leaf}"
}

resource "aci_leaf_interface_profile" "create_port_channel_leaf_profile" {
  for_each = {for pc_profile_row in local.local_data_porttable_pc_profile : pc_profile_row.leaf => pc_profile_row}
  name        = "L${each.value.leaf}_PC_IPR"
  description  = "Interface Profile Port-Channel for Leaf L${each.value.leaf}"
}

resource "aci_leaf_interface_profile" "create_virtual_port_channel_leaf_profile" {
  for_each = {for vpc_profile_row in local.local_data_porttable_vpc_profile : vpc_profile_row.leaf_vpc => vpc_profile_row}
  name        = "L${split("-",each.value.leaf_vpc)[0]}_L${split("-",each.value.leaf_vpc)[1]}_VPC_IPR"
  description  = "Interface Profile Virtual Port-Channel for Leaves L${split("-",each.value.leaf_vpc)[0]} and L${split("-",each.value.leaf_vpc)[1]}"
}

resource "aci_leaf_profile" "create_leaf_profile_sp" {
  for_each     = {for sp_profile_row in local.local_data_porttable_sp_profile : sp_profile_row.leaf => sp_profile_row}
  name         = "L${each.value.leaf}_SPR"
  description  = "Switch Profile for Leaf L${each.value.leaf}"
  leaf_selector {
    name                    = "L${each.value.leaf}_SWSEL"
    switch_association_type = "range"
    node_block {
      name  = "blk1"
      from_ = "${each.value.leaf}"
      to_   = "${each.value.leaf}"
    }
  }
  relation_infra_rs_acc_port_p = [aci_leaf_interface_profile.create_single_leaf_profile["${each.value.leaf}"].id]
}

resource "aci_leaf_profile" "create_leaf_profile_pc" {
  for_each     = {for pc_profile_row in local.local_data_porttable_pc_profile : pc_profile_row.leaf => pc_profile_row}
  name         = "L${each.value.leaf}_SPR"
  leaf_selector {
    name                    = "L${each.value.leaf}_SWSEL"
    switch_association_type = "range"
    node_block {
      name  = "blk1"
      from_ = "${each.value.leaf}"
      to_   = "${each.value.leaf}"
    }
  }
  relation_infra_rs_acc_port_p = [aci_leaf_interface_profile.create_port_channel_leaf_profile["${each.value.leaf}"].id]
}

resource "aci_leaf_profile" "create_leaf_profile_vpc" {
  for_each     = {for vpc_profile_row in local.local_data_porttable_vpc_profile : vpc_profile_row.leaf_vpc => vpc_profile_row}
  name         = "L${split("-",each.value.leaf_vpc)[0]}_L${split("-",each.value.leaf_vpc)[1]}_VPC_SPR"
  description  = "Switch Profile Virtual Port-Channel for Leaves L${split("-",each.value.leaf_vpc)[0]} and L${split("-",each.value.leaf_vpc)[1]}"
  leaf_selector {
    name                    = "L${split("-",each.value.leaf_vpc)[0]}_${split("-",each.value.leaf_vpc)[1]}_SWSEL"
    switch_association_type = "range"
    node_block {
      name  = "blk1"
      from_ = "${split("-",each.value.leaf_vpc)[0]}"
      to_   = "${split("-",each.value.leaf_vpc)[0]}"
    }
    node_block {
      name  = "blk2"
      from_ = "${split("-",each.value.leaf_vpc)[1]}"
      to_   = "${split("-",each.value.leaf_vpc)[1]}"
    }
  }
  relation_infra_rs_acc_port_p = [aci_leaf_interface_profile.create_virtual_port_channel_leaf_profile["${each.value.leaf_vpc}"].id]
}

resource "aci_rest" "create_leaf_profile_sp" {
  depends_on = [aci_leaf_profile.create_leaf_profile_sp]
  for_each = {for sp_profile_row in local.local_data_porttable_sp_profile : sp_profile_row.leaf => sp_profile_row}
  path       = "/api/node/mo/uni/infra/nprof-L${each.value.leaf}_SPR.json"
  payload = <<EOF
    infraRsAccPortP:
        attributes:
            tDn: "uni/infra/accportprof-L${each.value.leaf}_IPR"
            status: 'created,modified'
        children: []
  EOF
}

resource "aci_rest" "create_leaf_profile_pc" {
  depends_on = [aci_leaf_profile.create_leaf_profile_pc]
  for_each = {for pc_profile_row in local.local_data_porttable_pc_profile : pc_profile_row.leaf => pc_profile_row}
  path       = "/api/node/mo/uni/infra/nprof-L${each.value.leaf}_SPR.json"
  payload = <<EOF
    infraRsAccPortP:
        attributes:
            tDn: "uni/infra/accportprof-L${each.value.leaf}_PC_IPR"
            status: 'created,modified'
        children: []
  EOF
}

resource "aci_rest" "create_leaf_profile_vpc" {
  depends_on = [aci_leaf_profile.create_leaf_profile_vpc]
  for_each = {for vpc_profile_row in local.local_data_porttable_vpc_profile : vpc_profile_row.leaf_vpc => vpc_profile_row}
  path       = "/api/node/mo/uni/infra/nprof-L${split("-",each.value.leaf_vpc)[0]}_L${split("-",each.value.leaf_vpc)[1]}_VPC_SPR.json"
  payload = <<EOF
    infraRsAccPortP:
        attributes:
            tDn: "uni/infra/accportprof-L${split("-",each.value.leaf_vpc)[0]}_L${split("-",each.value.leaf_vpc)[1]}_VPC_IPR"
            status: 'created,modified'
        children: []
  EOF
}

resource "aci_access_port_selector" "create_port_selector_sp" {
  depends_on = [
    aci_leaf_interface_profile.create_single_leaf_profile,
    aci_leaf_profile.create_leaf_profile_sp,
    aci_rest.create_leaf_profile_sp,
  ]
  for_each = {for sp_port_block_row in local.local_data_porttable_sp_port_block : sp_port_block_row.pkey => sp_port_block_row}
  leaf_interface_profile_dn = aci_leaf_interface_profile.create_single_leaf_profile["${each.value.leaf}"].id
  name                      = "ETH1_${each.value.port}_PS"
  access_port_selector_type = "range"
  relation_infra_rs_acc_base_grp = aci_leaf_access_port_policy_group.create_standard_port_auto["link${lower(each.value.speed)}disable"].id
}
resource "aci_access_port_block" "create_port_block_sp" {
  depends_on = [aci_access_port_selector.create_port_selector_sp]
  for_each = {for sp_port_block_row in local.local_data_porttable_sp_port_block : sp_port_block_row.pkey => sp_port_block_row}
  access_port_selector_dn = aci_access_port_selector.create_port_selector_sp["${each.value.pkey}"].id
  name                    = "block2"
  from_card               = "1"
  from_port               = "${each.value.port}"
  to_card                 = "1"
  to_port                 = "${each.value.port}"
}

resource "aci_access_port_selector" "create_port_selector_pc" {
  depends_on = [
    aci_leaf_interface_profile.create_port_channel_leaf_profile,
    aci_leaf_profile.create_leaf_profile_pc,
    aci_rest.create_leaf_profile_pc,
  ]
  for_each = {for pc_port_block_row in local.local_data_porttable_pc_port_block : pc_port_block_row.pkey => pc_port_block_row}
  leaf_interface_profile_dn = aci_leaf_interface_profile.create_port_channel_leaf_profile["${each.value.leaf}"].id
  name                      = "ETH1_${each.value.port}_PS"
  access_port_selector_type = "range"
  relation_infra_rs_acc_base_grp = "uni/infra/funcprof/accbundle-PC_${upper(replace(each.value.device," ","_"))}_IPG"
}
resource "aci_access_port_block" "create_port_block_pc" {
  depends_on = [aci_access_port_selector.create_port_selector_pc]
  for_each = {for pc_port_block_row in local.local_data_porttable_pc_port_block : pc_port_block_row.pkey => pc_port_block_row}
  access_port_selector_dn = aci_access_port_selector.create_port_selector_pc["${each.value.pkey}"].id
  name                    = "block2"
  from_card               = "1"
  from_port               = "${each.value.port}"
  to_card                 = "1"
  to_port                 = "${each.value.port}"
}

resource "aci_access_port_selector" "create_port_selector_vpc" {
  depends_on = [
    aci_leaf_interface_profile.create_virtual_port_channel_leaf_profile,
    aci_leaf_profile.create_leaf_profile_vpc,
    aci_rest.create_leaf_profile_vpc,
  ]
  for_each = {for vpc_port_block_row in local.local_data_porttable_vpc_port_block : vpc_port_block_row.pkey => vpc_port_block_row}
  leaf_interface_profile_dn = aci_leaf_interface_profile.create_virtual_port_channel_leaf_profile["${each.value.leaf_vpc}"].id
  name                      = "ETH1_${each.value.port}_PS"
  access_port_selector_type = "range"
  relation_infra_rs_acc_base_grp = "uni/infra/funcprof/accbundle-VPC_${upper(replace(each.value.device," ","_"))}_IPG"
}
resource "aci_access_port_block" "create_port_block_vpc" {
  depends_on = [aci_access_port_selector.create_port_selector_vpc]
  for_each = {for vpc_port_block_row in local.local_data_porttable_vpc_port_block : vpc_port_block_row.pkey => vpc_port_block_row}
  access_port_selector_dn = aci_access_port_selector.create_port_selector_vpc["${each.value.pkey}"].id
  name                    = "block2"
  from_card               = "1"
  from_port               = "${each.value.port}"
  to_card                 = "1"
  to_port                 = "${each.value.port}"
}

resource "aci_rest" "assign_description_to_interfaces" {
  depends_on = [
    aci_access_port_selector.create_port_selector_sp,
    aci_access_port_selector.create_port_selector_pc,
    aci_access_port_selector.create_port_selector_vpc,
  ]
  for_each = {for port_desc in local.local_data_porttable_port_description : port_desc.id => port_desc}
  path       = "/api/node/mo/uni/infra/hpaths-${tostring(each.value.leaf)}_eth1_${tostring(each.value.port)}.json"
  payload = <<EOF
    infraHPathS:
        attributes:
            rn: hpaths-${tostring(each.value.leaf)}_eth1_${tostring(each.value.port)}
            dn: uni/infra/hpaths-${tostring(each.value.leaf)}_eth1_${tostring(each.value.port)}
            descr: ${each.value.device}-${each.value.comments}
            name: ${tostring(each.value.leaf)}_eth1_${tostring(each.value.port)}
        children:
            - infraRsHPathAtt:
                attributes:
                    dn: 'uni/infra/hpaths-${tostring(each.value.leaf)}_eth1_${tostring(each.value.port)}/rsHPathAtt-[topology/pod-${tostring(each.value.podid)}/paths-${tostring(each.value.leaf)}/pathep-[eth1/${tostring(each.value.port)}]]'
                    tDn: 'topology/pod-${tostring(each.value.podid)}/paths-${tostring(each.value.leaf)}/pathep-[eth1/${tostring(each.value.port)}]'
  EOF
}

output "eof_fabric_1" {
  depends_on = [
    aci_vlan_pool.configure_vlan_pool,
    aci_ranges.servers_stat_vlp_vlanpool,
    aci_ranges.l3_stat_vlp_vlanpool,
    aci_physical_domain.create_phy_dom,
    aci_l3_domain_profile.create_l3_dom,
    aci_attachable_access_entity_profile.create_aaep_phy_dom,
    aci_lldp_interface_policy.create_lldp_policies,
    aci_cdp_interface_policy.create_cdp_policies,
    aci_rest.create_link_level_auto,
    aci_rest.create_link_level_full,
    aci_rest.create_po_policy,
    aci_vpc_explicit_protection_group.create_vpc_groups,
    aci_leaf_access_port_policy_group.create_standard_port_auto,
    aci_leaf_access_port_policy_group.create_standard_port_full,
    aci_leaf_access_bundle_policy_group.create_vpc_policy_groups,
    aci_leaf_access_bundle_policy_group.create_pc_policy_groups,
    aci_leaf_interface_profile.create_single_leaf_profile,
    aci_leaf_interface_profile.create_port_channel_leaf_profile,
    aci_leaf_interface_profile.create_virtual_port_channel_leaf_profile,
    aci_leaf_profile.create_leaf_profile_sp,
    aci_leaf_profile.create_leaf_profile_pc,
    aci_leaf_profile.create_leaf_profile_vpc,
    aci_rest.create_leaf_profile_sp,
    aci_rest.create_leaf_profile_pc,
    aci_rest.create_leaf_profile_vpc,
    aci_access_port_selector.create_port_selector_sp,
    aci_access_port_block.create_port_block_sp,
    aci_access_port_selector.create_port_selector_pc,
    aci_access_port_block.create_port_block_pc,
    aci_access_port_selector.create_port_selector_vpc,
    aci_access_port_block.create_port_block_vpc,
    aci_rest.assign_description_to_interfaces,
  ]
  value = "end-of-fabric-1"
}