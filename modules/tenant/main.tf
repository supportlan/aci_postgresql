# variable "target_group_depends_on_1" {
#   type    = any
#   default = null
# }

locals {
  local_data_ipplan_tenant = jsondecode(file("${path.root}/ipplan_tenant.json"))
  local_data_ipplan_appname = jsondecode(file("${path.root}/ipplan_appname.json"))
  local_data_ipplan_vrf = jsondecode(file("${path.root}/ipplan_vrf.json"))
  local_data_ipplan_bridge_domain = jsondecode(file("${path.root}/ipplan_bridge_domain.json"))
  local_data_ipplan_subnet_gateway = jsondecode(file("${path.root}/ipplan_subnet_gateway.json"))
  local_data_ipplan_epg = jsondecode(file("${path.root}/ipplan_epg.json"))
  local_data_ipplan_tenant_unique = jsondecode(file("${path.root}/ipplan_tenant_unique.json"))
}

resource "aci_tenant" "create_tenant" {
  for_each = {for tenant_name in local.local_data_ipplan_tenant : tenant_name.id => tenant_name}
  name        = "${upper(each.value.tenant)}"
  name_alias  = "${upper(each.value.tenant)}"
}

resource "aci_application_profile" "create_app_profile" {
  depends_on = [aci_tenant.create_tenant]
  for_each = {for app_name in local.local_data_ipplan_appname : app_name.id => app_name}
  tenant_dn   = aci_tenant.create_tenant[each.value.id].id
  name        = "${upper(each.value.appname)}"
  name_alias  = "${upper(each.value.appname)}"
  description = "${upper(each.value.appname)} deployed using Terraform"
}

resource "aci_vrf" "create_vrf" {
  depends_on = [aci_application_profile.create_app_profile]
  for_each = {for vrf_name in local.local_data_ipplan_vrf : vrf_name.id => vrf_name}
  tenant_dn = aci_tenant.create_tenant[each.value.id].id
  name      = "${upper(each.value.vrf_aci_name)}"
  name_alias  = "${upper(each.value.vrf_aci_name)}"
  description            = "${upper(each.value.vrf_aci_name)} deployed using Terraform"
  bd_enforced_enable     = "no"
  ip_data_plane_learning = "enabled"
  knw_mcast_act          = "permit"
  pc_enf_dir             = "ingress"
  pc_enf_pref            = "unenforced"
}

resource "aci_rest" "vrf_prefered_group" {
  depends_on = [aci_vrf.create_vrf]
  for_each = {for vrf_name in local.local_data_ipplan_vrf : vrf_name.id => vrf_name}
  path       = "/api/node/mo/uni/tn-${upper(each.value.tenant)}/ctx-${upper(each.value.vrf_aci_name)}/any.json"
  payload = <<EOF
    vzAny:
      attributes:
        dn: "uni/tn-${upper(each.value.tenant)}/ctx-${upper(each.value.vrf_aci_name)}/any"
        prefGrMemb: "enabled"
  EOF
}

resource "aci_bridge_domain" "create_bd" {
  depends_on = [aci_vrf.create_vrf]
  for_each = {for bd_name in local.local_data_ipplan_bridge_domain : bd_name.id => bd_name}
  tenant_dn                      = aci_tenant.create_tenant[each.value.id].id
  name                           = "${upper(each.value.bd)}"
  name_alias                     = "${upper(each.value.bd)}"
  arp_flood                      = "yes"
  description                    = "BD for VLAN ${upper(each.value.bd)}"
  unk_mac_ucast_act              = "proxy"
  unk_mcast_act                  = "flood"
  multi_dst_pkt_act              = "bd-flood"
  unicast_route                  = "yes"
  host_based_routing             = "yes"
  mcast_allow                    = "yes"
  limit_ip_learn_to_subnets      = "no"
  relation_fv_rs_ctx             = aci_vrf.create_vrf[each.value.id].id
}

resource "aci_subnet" "attach_subnet_to_bd" {
  depends_on = [aci_bridge_domain.create_bd]
  for_each = {for bd_name in local.local_data_ipplan_subnet_gateway : bd_name.id => bd_name}
  parent_dn = aci_bridge_domain.create_bd[each.value.id].id
  name_alias       = "VLAN_${tostring(each.value.vlan_id)}_subnet"
  scope            = ["public"]
  ip               = "${each.value.subnet_gw}"
  description      = "VLAN_${tostring(each.value.vlan_id)}_subnet"
}

resource "aci_application_epg" "create_epg" {
  depends_on = [
    aci_application_profile.create_app_profile,
    aci_bridge_domain.create_bd,
  ]
  for_each = {for epg_name in local.local_data_ipplan_epg : epg_name.id => epg_name}
  application_profile_dn  = aci_application_profile.create_app_profile[each.value.id].id
  name                    = "${upper(each.value.epg)}"
  name_alias              = "${upper(each.value.epg)}"
  pc_enf_pref             = "unenforced"
  pref_gr_memb            = "include"
  relation_fv_rs_bd       = aci_bridge_domain.create_bd[each.value.id].id
}

resource "aci_physical_domain" "create_phy_dom" {
  name        = "SERVERS_PHY_DOM"
  name_alias  = "SERVERS_PHY_DOM"
}

resource "aci_l3_domain_profile" "create_l3out_dom" {
  name        = "L3DCI_L3O_DOM"
  name_alias  = "L3DCI_L3O_DOM"
}

resource "aci_epg_to_domain" "attach_dom_epg" {
  depends_on = [
    aci_application_epg.create_epg,
    aci_physical_domain.create_phy_dom,
    aci_l3_domain_profile.create_l3out_dom,
  ]
  for_each = {for epg_name in local.local_data_ipplan_epg : epg_name.id => epg_name}
  application_epg_dn    = aci_application_epg.create_epg[each.value.id].id
  tdn                   = aci_physical_domain.create_phy_dom.id
}

resource "aci_bgp_timers" "configure_bgp_timers" {
  depends_on = [aci_epg_to_domain.attach_dom_epg]
  for_each = {
    for tenant_name in local.local_data_ipplan_tenant : tenant_name.id => tenant_name
  }
  tenant_dn    = aci_tenant.create_tenant[each.value.id].id
  description  = "BGP Timers for ${var.company_name} L3OUT"
  name         = "${var.company_name}_BGP_TIMERS"
  gr_ctrl      = "helper"
  hold_intvl   = var.bgp_holdtime
  ka_intvl     = var.bgp_keepalive
}

resource "aci_vrf" "assign_bgp_timers_vrf" {
  depends_on = [aci_bgp_timers.configure_bgp_timers]
  for_each = {for vrf_name in local.local_data_ipplan_vrf : vrf_name.id => vrf_name}
  tenant_dn = aci_tenant.create_tenant[each.value.id].id
  name      = "${upper(each.value.vrf_aci_name)}"
  relation_fv_rs_bgp_ctx_pol  = aci_bgp_timers.configure_bgp_timers[each.value.id].id
}

resource "aci_rest" "create_bfd_profile" {
  depends_on = [aci_vrf.assign_bgp_timers_vrf]
  for_each   = {for tenant_name in local.local_data_ipplan_tenant_unique : tenant_name.tenant => tenant_name}
  path       = "/api/node/mo/uni/tn-${upper(each.value.tenant)}/bfdIfPol-${var.company_name}_BFD_PROF.json"
  payload = <<EOF
    bfdIfPol:
      attributes:
        dn: "uni/tn-${upper(each.value.tenant)}/bfdIfPol-${var.company_name}_BFD_PROF"
        minRxIntvl: '250'
        minTxIntvl: '250'
        name: "${var.company_name}_BFD_PROF"
        rn: "bfdIfPol-${var.company_name}_BFD_PROF"
        status: created,modified
      children: []
  EOF
}

output "eof_tenant_1" {
  depends_on = [aci_vrf.assign_bgp_timers_vrf]
  value = "end-of-tenant-1"
}

