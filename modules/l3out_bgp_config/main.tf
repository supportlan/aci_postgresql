variable "target_group_depends_on_5" {
  type    = any
  default = null
}

locals {
  local_data_ipplan_l3outconfiguration_global = jsondecode(file("${path.root}/ipplan_l3outconfiguration_global.json"))  
}

### GLOBAL L3OUT CONFIG

resource "aci_l3_outside" "l3out_config_global" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                     = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  tenant_dn                    = "uni/tn-${each.value.tenant}"
  relation_l3ext_rs_ectx       = "uni/tn-${each.value.tenant}/ctx-${each.value.vrf_aci_name}"
  name                         = "${each.value.l3out_name}"
  name_alias                   = "${each.value.l3out_name}"
  relation_l3ext_rs_l3_dom_att = var.l3domain_id
}

resource "aci_logical_node_profile" "create_logical_node_profile" {
  depends_on = [var.target_group_depends_on_5]  
  for_each            = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  l3_outside_dn       =  aci_l3_outside.l3out_config_global[each.value.id].id
  name                = "${upper(each.value.vrf_name)}_NODEPR"
  name_alias          = "${upper(each.value.vrf_name)}_NODEPR"
}

resource "aci_external_network_instance_profile" "l3out_external_epg" {
  depends_on = [var.target_group_depends_on_5]  
  for_each            = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  l3_outside_dn       = aci_l3_outside.l3out_config_global[each.value.id].id
  name                = "${upper(each.value.tenant)}_${upper(each.value.vrf_name)}_EXTEPG"
  name_alias          = "${upper(each.value.tenant)}_${upper(each.value.vrf_name)}_EXTEPG"
}

resource "aci_l3_ext_subnet" "create_l3out_extepg_subnet" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                             = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  external_network_instance_profile_dn = aci_external_network_instance_profile.l3out_external_epg[each.value.id].id
  ip                                   = "0.0.0.0/0"
  scope                                = ["import-security"]
}

resource "aci_logical_node_to_fabric_node" "pod1_nodeA_logical_node" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  tdn                     = "topology/pod-${var.id_pod1_l3out}/node-${split("-",var.pod1_border_leaf)[0]}"
  rtr_id                  = "${var.rid_pod1_aci1_dc1}"
  logical_node_profile_dn = aci_logical_node_profile.create_logical_node_profile[each.value.id].id
  rtr_id_loop_back        = "no"
}

resource "aci_logical_node_to_fabric_node" "pod1_nodeB_logical_node" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  tdn                     = "topology/pod-${var.id_pod1_l3out}/node-${split("-",var.pod1_border_leaf)[1]}"
  rtr_id                  = "${var.rid_pod1_aci2_dc1}"
  logical_node_profile_dn = aci_logical_node_profile.create_logical_node_profile[each.value.id].id
  rtr_id_loop_back        = "no"
}


resource "aci_logical_interface_profile" "create_interface_profile_l3out" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  logical_node_profile_dn = aci_logical_node_profile.create_logical_node_profile[each.value.id].id
  name                    = "${upper(each.value.vrf_name)}_INTPROF"
  name_alias              = "${upper(each.value.vrf_name)}_INTPROF"
}

### L3OUT PATH AND VPC FOR DC

resource "aci_l3out_path_attachment" "attach_vpc_border_leaf_l3out_dc_pod1_active" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                     = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  logical_interface_profile_dn = aci_logical_interface_profile.create_interface_profile_l3out[each.value.id].id
  target_dn                    = "topology/pod-${var.id_pod1_l3out}/protpaths-${var.pod1_border_leaf}/pathep-[${var.vpc_l3out_sitea_primary_dc}]"
  if_inst_t                    = "ext-svi"
  encap                        = "vlan-${each.value.l3out_vlan_id}"
  mtu                          = "1500"
  mode                         = "regular"
}

resource "aci_l3out_path_attachment" "attach_vpc_border_leaf_l3out_dc_pod1_passive" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                     = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  logical_interface_profile_dn = aci_logical_interface_profile.create_interface_profile_l3out[each.value.id].id
  target_dn                    = "topology/pod-${var.id_pod1_l3out}/protpaths-${var.pod1_border_leaf}/pathep-[${var.vpc_l3out_sitea_secondary_dc}]"
  if_inst_t                    = "ext-svi"
  encap                        = "vlan-${each.value.l3out_vlan_id}"
  mtu                          = "1500"
  mode                         = "regular"
}

resource "aci_l3out_vpc_member" "ip_address_pod1_l3out_dc_side_A_active" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                     = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  leaf_port_dn                 = aci_l3out_path_attachment.attach_vpc_border_leaf_l3out_dc_pod1_active[each.value.id].id
  side                         = "A"
  addr                         = "${each.value.pod1_aci1}"
}

resource "aci_l3out_vpc_member" "ip_address_pod1_l3out_dc_side_A_passive" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                     = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  leaf_port_dn                 = aci_l3out_path_attachment.attach_vpc_border_leaf_l3out_dc_pod1_passive[each.value.id].id
  side                         = "A"
  addr                         = "${each.value.pod1_aci1}"
}

resource "aci_l3out_vpc_member" "ip_address_pod1_l3out_dc_side_B_active" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                     = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  leaf_port_dn                 = aci_l3out_path_attachment.attach_vpc_border_leaf_l3out_dc_pod1_active[each.value.id].id
  side                         = "B"
  addr                         = "${each.value.pod1_aci2}"
}

resource "aci_l3out_vpc_member" "ip_address_pod1_l3out_dc_side_B_passive" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                     = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  leaf_port_dn                 = aci_l3out_path_attachment.attach_vpc_border_leaf_l3out_dc_pod1_passive[each.value.id].id
  side                         = "B"
  addr                         = "${each.value.pod1_aci2}"
}


### BGP GLOBAL CONFIGURATION
resource "aci_l3out_bgp_external_policy" "enable_bgp_l3out" {
  depends_on = [var.target_group_depends_on_5]  
  for_each                     = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  l3_outside_dn                = aci_l3_outside.l3out_config_global[each.value.id].id
}

resource "aci_bgp_timers" "l3out_add_bgp_timers_pod1" {
  depends_on = [var.target_group_depends_on_5]  
  for_each     = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  tenant_dn    = "uni/tn-${upper(each.value.tenant)}"
  name         = "${var.company_name}_BGP_TIMERS"
  annotation   = "orchestrator:terraform"
  gr_ctrl      = "helper"
  hold_intvl   = "17"
  ka_intvl     = "5"
  name_alias   = "${var.company_name}_BGP_TIMERS"
  stale_intvl  = "15"
}

resource "aci_rest" "assign_bfd_to_l3out_pod1" {
  depends_on = [
    aci_l3_outside.l3out_config_global,
    aci_logical_node_profile.create_logical_node_profile,
    aci_external_network_instance_profile.l3out_external_epg,
    aci_l3_ext_subnet.create_l3out_extepg_subnet,
    aci_logical_node_to_fabric_node.pod1_nodeA_logical_node,
    aci_logical_node_to_fabric_node.pod1_nodeB_logical_node,
    aci_logical_interface_profile.create_interface_profile_l3out,
    aci_l3out_path_attachment.attach_vpc_border_leaf_l3out_dc_pod1_active,
    aci_l3out_path_attachment.attach_vpc_border_leaf_l3out_dc_pod1_passive,
    aci_l3out_vpc_member.ip_address_pod1_l3out_dc_side_A_active,
    aci_l3out_vpc_member.ip_address_pod1_l3out_dc_side_A_passive,
    aci_l3out_vpc_member.ip_address_pod1_l3out_dc_side_B_active,
    aci_l3out_vpc_member.ip_address_pod1_l3out_dc_side_B_passive,
  ]
  for_each   = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  path       = "/api/node/mo/uni/tn-${upper(each.value.tenant)}/out-${upper(tostring(each.value.l3out_name))}.json"
  payload = <<EOF
    bfdIfP:
      attributes:
        dn: uni/tn-${upper(each.value.tenant)}/out-${upper(tostring(each.value.l3out_name))}/lnodep-${upper(each.value.vrf_name)}_NODEPR/lifp-${upper(each.value.vrf_name)}_INTPROF/bfdIfP
        rn: bfdIfP
        status: created,modified
      children:
      - bfdRsIfPol:
          attributes:
            status: created,modified,modified
            tnBfdIfPolName: "${var.company_name}_BFD_PROF"
          children: []
  EOF
}

### BGP PEER CONFIG DC

resource "aci_bgp_peer_connectivity_profile" "bgp_peer_pod1_to_pod1_fw_active_dc" {
  depends_on = [aci_bgp_timers.l3out_add_bgp_timers_pod1,aci_rest.assign_bfd_to_l3out_pod1]
  for_each            = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  parent_dn           = aci_l3out_path_attachment.attach_vpc_border_leaf_l3out_dc_pod1_active[each.value.id].id
  addr                = "${each.value.l3out_ip_fw_1}"
  addr_t_ctrl         = ["af-ucast"]
  ctrl                = ["nh-self","send-com","send-ext-com"]
  password            = "${var.bgp_password}"
  peer_ctrl           = ["bfd"]
  as_number           = "${var.bgp_asn_dc_pod1}"
  local_asn           = "${tostring(each.value.bgp_asn_aci)}"
  local_asn_propagate = "replace-as"
  admin_state         = "enabled"

  relation_bgp_rs_peer_to_profile {
    direction = "import"
    target_dn = "uni/tn-${each.value.tenant}/prof-RM_${upper(each.value.vrf_name)}_P_IN"
  }
  relation_bgp_rs_peer_to_profile {
    direction = "export"
    target_dn = "uni/tn-${each.value.tenant}/prof-RM_${upper(each.value.vrf_name)}_P_OUT"
  }
}

resource "aci_bgp_peer_connectivity_profile" "bgp_peer_pod1_to_pod1_fw_passive_dc" {
  depends_on = [aci_bgp_timers.l3out_add_bgp_timers_pod1,aci_rest.assign_bfd_to_l3out_pod1]
  for_each            = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  parent_dn           = aci_l3out_path_attachment.attach_vpc_border_leaf_l3out_dc_pod1_passive[each.value.id].id
  addr                = "${each.value.l3out_ip_fw_1}"
  addr_t_ctrl         = ["af-ucast"]
  ctrl                = ["nh-self","send-com","send-ext-com"]
  password            = "${var.bgp_password}"
  peer_ctrl           = ["bfd"]
  as_number           = "${var.bgp_asn_dc_pod1}"
  local_asn           = "${tostring(each.value.bgp_asn_aci)}"
  local_asn_propagate = "replace-as"
  admin_state         = "enabled"

  relation_bgp_rs_peer_to_profile {
    direction = "import"
    target_dn = "uni/tn-${each.value.tenant}/prof-RM_${upper(each.value.vrf_name)}_P_IN"
  }
  relation_bgp_rs_peer_to_profile {
    direction = "export"
    target_dn = "uni/tn-${each.value.tenant}/prof-RM_${upper(each.value.vrf_name)}_P_OUT"
  }
}

resource "aci_rest" "assign_bgp_timers_to_l3out_pod1" {
  depends_on = [aci_bgp_timers.l3out_add_bgp_timers_pod1,aci_rest.assign_bfd_to_l3out_pod1]
  for_each   = {for l3out_pod in local.local_data_ipplan_l3outconfiguration_global : l3out_pod.id => l3out_pod}
  path       = "/api/node/mo/uni/tn-${upper(each.value.tenant)}/out-${upper(tostring(each.value.l3out_name))}/lnodep-${upper(each.value.vrf_name)}_NODEPR/protp.json"
  payload = <<EOF
    bgpProtP:
        attributes:
            dn: uni/tn-${upper(each.value.tenant)}/out-${upper(tostring(each.value.l3out_name))}/lnodep-${upper(each.value.vrf_name)}_NODEPR/protp
            rn: protp
            status: 'created,modified'
        children:
            - bgpRsBgpNodeCtxPol:
                    attributes:
                        tnBgpCtxPolName: "${var.company_name}_BGP_TIMERS"
                        status: 'created,modified'
                    children: []
  EOF
}


