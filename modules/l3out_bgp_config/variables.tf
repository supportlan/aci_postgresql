variable "username" {
	default = "adm_api"
}

variable "cert_name" {
	default = "adm_api"
}

variable "private_key"{
	default = "./adm_api.key"
}

variable "url" {
	default = "https://192.168.10.1"
}

variable "company_name" {
	default = "SAQ"
}

variable "bgp_holdtime" {
	default = "17"
}

variable "bgp_keepalive" {
	default = "5"
}

variable "id_pod1_bgp" {
	default = "1"
}

variable "id_pod1_l3out" {
	default = "1"
}

variable "pod1_border_leaf" {
	default = "1431-1441"
}

variable "vpc_l3out_sitea_primary_dc" {
	default = "VPC_SMY_DC_FW01_IPG"
}

variable "vpc_l3out_sitea_secondary_dc" {
	default = "VPC_SMY_DC_FW02_IPG"
}

variable "bgp_asn_dc_pod1" {
	default = "65199"
}

variable "bgp_password" {
	default = "IxE3en7dpiUyEg"
}

variable "rid_pod1_aci1_dc1" {
	default = "100.80.46.1"
}

variable "rid_pod1_aci2_dc1" {
	default = "100.80.46.2"
}

variable "l3domain_id" {
	default = "uni/l3dom-L3DCI_L3O_DOM"
}
