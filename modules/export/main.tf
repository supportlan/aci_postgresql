# data "curl" "ipplan_tenant" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_tenant"
# }

# data "curl" "ipplan_tenant_unique" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_tenant_unique"
# }

# data "curl" "ipplan_appname" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_appname"
# }

# data "curl" "ipplan_vrf" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_vrf"
# }

# data "curl" "ipplan_bridge_domain" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_bridge_domain"
# }

# data "curl" "ipplan_subnet_gateway" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_subnet_gateway"
# }

# data "curl" "ipplan_epg" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_epg"
# }

# data "curl" "ipplan_l3outconfiguration_global" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_l3outconfiguration_global"
# }

# data "curl" "ipplan_bgp_per_vrf" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_bgp_per_vrf"
# }

# data "curl" "ipplan_phy_vlan_pool" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_phy_vlan_pool"
# }

# data "curl" "ipplan_l3_vlan_pool" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_l3_vlan_pool"
# }

# data "curl" "ipplan_aaep" {
#   http_method = "GET"
#   uri = "http://localhost:3000/ipplan_aaep"
# }

# data "curl" "porttable_vpc_groups" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_vpc_groups"
# }

# data "curl" "porttable_vpc_policy_groups" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_vpc_policy_groups"
# }

# data "curl" "porttable_pc_policy_groups" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_pc_policy_groups"
# }

# data "curl" "porttable_sp_profile" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_sp_profile"
# }

# data "curl" "porttable_pc_profile" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_pc_profile"
# }

# data "curl" "porttable_vpc_profile" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_vpc_profile"
# }

# data "curl" "porttable_switch_sp_pc_profile" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_switch_sp_pc_profile"
# }

# data "curl" "porttable_switch_sp_pc_profile_detail" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_switch_sp_pc_profile_detail"
# }

# data "curl" "porttable_sp_port_block" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_sp_port_block"
# }

# data "curl" "porttable_pc_port_block" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_pc_port_block"
# }

# data "curl" "porttable_vpc_port_block" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_vpc_port_block"
# }

# data "curl" "porttable_port_description" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_port_description"
# }

# data "curl" "porttable_attach_sp_access" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_attach_sp_access"
# }

# data "curl" "porttable_attach_sp_native" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_attach_sp_native"
# }

# data "curl" "porttable_attach_sp_trunk" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_attach_sp_trunk"
# }

# data "curl" "porttable_attach_pc_access" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_attach_pc_access"
# }

# data "curl" "porttable_attach_pc_native" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_attach_pc_native"
# }

# data "curl" "porttable_attach_pc_trunk" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_attach_pc_trunk"
# }

# data "curl" "porttable_attach_vpc_access" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_attach_vpc_access"
# }

# data "curl" "porttable_attach_vpc_native" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_attach_vpc_native"
# }

# data "curl" "porttable_attach_vpc_trunk" {
#   http_method = "GET"
#   uri = "http://localhost:3000/porttable_attach_vpc_trunk"
# }

resource "null_resource" "export_data" {
 provisioner "local-exec" {
    command = "/bin/bash export_postgresql.sh"
  }
}


output "eof_export_data_json" {
  depends_on = [null_resource.export_data]
  value = "end-of-export-1"
}

