variable "username" {
	default = "adm_api"
}

variable "cert_name" {
	default = "adm_api"
}

variable "private_key"{
	default = "./adm_api.key"
}

variable "url" {
	default = "https://192.168.10.1"
}

variable "company_name" {
	default = "SAQ"
}

variable "bgp_holdtime" {
	default = "17"
}

variable "bgp_keepalive" {
	default = "5"
}

variable "pod1_name" {
	default = "SAQ"
}

variable "fabric_bgp_asn" {
	default = "65199"
}