variable "target_group_depends_on_3" {
  type    = any
  default = null
}

locals {
  local_data_ipplan_tenant = jsondecode(file("${path.root}/ipplan_tenant.json"))
  local_data_ipplan_bgp_per_vrf = jsondecode(file("${path.root}/ipplan_bgp_per_vrf.json"))
  local_data_ipplan_tenant_unique = jsondecode(file("${path.root}/ipplan_tenant_unique.json"))  
}

resource "aci_match_rule" "rule_default" {
  depends_on = [var.target_group_depends_on_3]  
  for_each          = {for tenant_id in local.local_data_ipplan_tenant_unique : tenant_id.tenant => tenant_id}
  tenant_dn         = "uni/tn-${upper(each.value.tenant)}"
  name              = "RM_MATCH_PL_L3O_DEF"
  annotation        = "orchestrator:terraform"
}

resource "aci_match_route_destination_rule" "destination_default_route" {
  depends_on = [var.target_group_depends_on_3]  
  for_each          = {for tenant_id in local.local_data_ipplan_tenant_unique : tenant_id.tenant => tenant_id}
  match_rule_dn         = aci_match_rule.rule_default[each.value.tenant].id
  ip                    = "0.0.0.0"
  aggregate             = "no"
  annotation            = "orchestrator:terraform"
  greater_than_mask     = "0"
  less_than_mask        = "0"
}

resource "aci_match_rule" "rule_all_subnets_rfc1918" {
  depends_on = [var.target_group_depends_on_3]  
  for_each          = {for tenant_id in local.local_data_ipplan_tenant_unique : tenant_id.tenant => tenant_id}
  tenant_dn         = "uni/tn-${upper(each.value.tenant)}"
  name              = "RM_MATCH_PL_L3O_ALL"
  annotation        = "orchestrator:terraform"
}

resource "aci_match_route_destination_rule" "destination_rfc1918_172" {
  depends_on = [var.target_group_depends_on_3]  
  for_each          = {for tenant_id in local.local_data_ipplan_tenant_unique : tenant_id.tenant => tenant_id}
  match_rule_dn         = aci_match_rule.rule_all_subnets_rfc1918[each.value.tenant].id
  ip                    = "172.16.0.0/12"
  aggregate             = "yes"
  annotation            = "orchestrator:terraform"
  greater_than_mask     = "0"
  less_than_mask        = "32"
}

resource "aci_match_route_destination_rule" "destination_rfc1918_192" {
  depends_on = [var.target_group_depends_on_3]  
  for_each          = {for tenant_id in local.local_data_ipplan_tenant_unique : tenant_id.tenant => tenant_id}
  match_rule_dn         = aci_match_rule.rule_all_subnets_rfc1918[each.value.tenant].id
  ip                    = "192.168.0.0/16"
  aggregate             = "yes"
  annotation            = "orchestrator:terraform"
  greater_than_mask     = "0"
  less_than_mask        = "32"
}

resource "aci_match_route_destination_rule" "destination_rfc1918_10" {
  depends_on = [var.target_group_depends_on_3]  
  for_each              = {for tenant_id in local.local_data_ipplan_tenant_unique : tenant_id.tenant => tenant_id}
  match_rule_dn         = aci_match_rule.rule_all_subnets_rfc1918[each.value.tenant].id
  ip                    = "10.0.0.0/8"
  aggregate             = "yes"
  annotation            = "orchestrator:terraform"
  greater_than_mask     = "0"
  less_than_mask        = "32"
}

resource "aci_action_rule_profile" "rmap_set_rule_outbound" {
  depends_on = [var.target_group_depends_on_3]  
  for_each = {for bgp_id in local.local_data_ipplan_bgp_per_vrf : bgp_id.id => bgp_id}
  tenant_dn       = "uni/tn-${upper(each.value.tenant)}"
  description     = "Route-map set rule for vrf ${upper(each.value.vrf_name)}"
  name            = "RM_SET_${upper(each.value.tenant)}_COMM_P_OUT"
  annotation      = "orchestrator:terraform"
  name_alias      = "RM_SET_${upper(each.value.tenant)}_COMM_P_OUT"
  set_communities = {
    community = "regular:as2-nn2:${upper(each.value.bgp_asn_aci)}:100"
    criteria  = "replace"
  }
}

resource "aci_action_rule_profile" "rmap_set_rule_inbound" {
  depends_on = [var.target_group_depends_on_3]  
  for_each        = {for tenant_id in local.local_data_ipplan_tenant_unique : tenant_id.tenant => tenant_id}
  tenant_dn       = "uni/tn-${upper(each.value.tenant)}"
  description     = "Route-map set rule for vrf ${upper(each.value.tenant)}"
  name            = "RM_SET_${upper(each.value.tenant)}_COMM_P_IN"
  annotation      = "orchestrator:terraform"
  name_alias      = "RM_SET_${upper(each.value.tenant)}_COMM_P_IN"
  set_communities = {
    community = "regular:as2-nn2:${var.fabric_bgp_asn}:900"
    criteria  = "replace"
  }
}

resource "aci_match_rule" "rule_match_community" {
  depends_on = [var.target_group_depends_on_3]  
  for_each          = {for tenant_id in local.local_data_ipplan_tenant_unique : tenant_id.tenant => tenant_id}
  tenant_dn         = "uni/tn-${upper(each.value.tenant)}"
  name              = "RM_MATCH_COMM_IN"
  annotation        = "orchestrator:terraform"
}

resource "aci_match_community_terms" "match_community_learned" {
  depends_on = [var.target_group_depends_on_3]  
  for_each          = {for tenant_id in local.local_data_ipplan_tenant_unique : tenant_id.tenant => tenant_id}
  match_rule_dn     = aci_match_rule.rule_match_community[each.value.tenant].id
  name              = "MATCH_COMM_LEARNED_IN"
  annotation        = "orchestrator:terraform"
  match_community_factors {
    community       = "regular:as2-nn2:${var.fabric_bgp_asn}:900"
  }
}

resource "aci_route_control_profile" "rmap_inbound" {
  depends_on = [var.target_group_depends_on_3]  
  for_each = {for bgp_id in local.local_data_ipplan_bgp_per_vrf : bgp_id.id => bgp_id}
  parent_dn                  = "uni/tn-${upper(each.value.tenant)}"
  name                       = "RM_${upper(each.value.vrf_name)}_P_IN"
  annotation                 = "orchestrator:terraform"
  description                = "Route-Map IN for vrf ${upper(each.value.vrf_name)}"
  name_alias                 = "RM_${upper(each.value.vrf_name)}_P_IN"
  route_control_profile_type = "global"
}

resource "aci_route_control_profile" "rmap_outbound" {
  depends_on = [var.target_group_depends_on_3]  
  for_each = {for bgp_id in local.local_data_ipplan_bgp_per_vrf : bgp_id.id => bgp_id}
  parent_dn                  = "uni/tn-${upper(each.value.tenant)}"
  name                       = "RM_${upper(each.value.vrf_name)}_P_OUT"
  annotation                 = "orchestrator:terraform"
  description                = "Route-Map OUT for vrf ${upper(each.value.vrf_name)}"
  name_alias                 = "RM_${upper(each.value.vrf_name)}_P_OUT"
  route_control_profile_type = "global"
}

resource "aci_route_control_context" "control-inbound" {
  depends_on = [var.target_group_depends_on_3]  
  for_each                           = {for bgp_id in local.local_data_ipplan_bgp_per_vrf : bgp_id.id => bgp_id}
  route_control_profile_dn           = aci_route_control_profile.rmap_inbound[each.value.id].id
  name                               = "RM_CTX_L3O_P_IN_0"
  action                             = "permit"
  annotation                         = "orchestrator:terraform"
  order                              = "0"
  set_rule                           = aci_action_rule_profile.rmap_set_rule_inbound[each.value.tenant].id
  relation_rtctrl_rs_ctx_p_to_subj_p = [
                                        aci_match_rule.rule_default[each.value.tenant].id
                                       ]
}

resource "aci_route_control_context" "control-outbound-0" {
  depends_on = [var.target_group_depends_on_3]  
  for_each                           = {for bgp_id in local.local_data_ipplan_bgp_per_vrf : bgp_id.id => bgp_id}
  route_control_profile_dn           = aci_route_control_profile.rmap_outbound[each.value.id].id
  name                               = "RM_CTX_L3O_P_OUT_0"
  action                             = "deny"
  annotation                         = "orchestrator:terraform"
  order                              = "0"
  relation_rtctrl_rs_ctx_p_to_subj_p = [
                                        aci_match_rule.rule_match_community[each.value.tenant].id
                                       ]
}

resource "aci_route_control_context" "control-outbound-1" {
  depends_on = [var.target_group_depends_on_3]  
  for_each                           = {for bgp_id in local.local_data_ipplan_bgp_per_vrf : bgp_id.id => bgp_id}
  route_control_profile_dn           = aci_route_control_profile.rmap_outbound[each.value.id].id
  name                               = "RM_CTX_L3O_P_OUT_1"
  action                             = "permit"
  annotation                         = "orchestrator:terraform"
  order                              = "1"
  set_rule                           = aci_action_rule_profile.rmap_set_rule_outbound[each.value.id].id
  relation_rtctrl_rs_ctx_p_to_subj_p = [
                                        aci_match_rule.rule_all_subnets_rfc1918[each.value.tenant].id
                                       ]
}

output "eof_l3out" {
  depends_on = [
    aci_match_rule.rule_default,
    aci_match_route_destination_rule.destination_default_route,
    aci_match_rule.rule_all_subnets_rfc1918,
    aci_match_route_destination_rule.destination_rfc1918_172,
    aci_match_route_destination_rule.destination_rfc1918_192,
    aci_match_route_destination_rule.destination_rfc1918_10,
    aci_action_rule_profile.rmap_set_rule_outbound,
    aci_action_rule_profile.rmap_set_rule_inbound,
    aci_match_rule.rule_match_community,
    aci_match_community_terms.match_community_learned,
    aci_route_control_profile.rmap_inbound,
    aci_route_control_profile.rmap_outbound,
    aci_route_control_context.control-inbound,
    aci_route_control_context.control-outbound-0,
    aci_route_control_context.control-outbound-1,
  ]
  value = "end-of-l3out"
}
