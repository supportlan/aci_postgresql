variable "target_group_depends_on_3" {
  type    = any
  default = null
}
variable "target_group_depends_on_4" {
  type    = any
  default = null
}

locals {
  local_data_porttable_attach_sp_access = jsondecode(file("${path.root}/porttable_attach_sp_access.json"))
  local_data_porttable_attach_sp_native = jsondecode(file("${path.root}/porttable_attach_sp_native.json"))
  local_data_porttable_attach_sp_trunk = jsondecode(file("${path.root}/porttable_attach_sp_trunk.json"))
  local_data_porttable_attach_pc_access = jsondecode(file("${path.root}/porttable_attach_pc_access.json"))
  local_data_porttable_attach_pc_native = jsondecode(file("${path.root}/porttable_attach_pc_native.json"))
  local_data_porttable_attach_pc_trunk = jsondecode(file("${path.root}/porttable_attach_pc_trunk.json"))
  local_data_porttable_attach_vpc_access = jsondecode(file("${path.root}/porttable_attach_vpc_access.json"))
  local_data_porttable_attach_vpc_native = jsondecode(file("${path.root}/porttable_attach_vpc_native.json"))
  local_data_porttable_attach_vpc_trunk = jsondecode(file("${path.root}/porttable_attach_vpc_trunk.json"))
}

resource "aci_epg_to_static_path" "attach_epg_sp_access" {
  depends_on = [var.target_group_depends_on_3,var.target_group_depends_on_4]
  for_each            = {for attach_access in local.local_data_porttable_attach_sp_access : attach_access.pkey => attach_access}
  application_epg_dn  = "uni/tn-${upper(each.value.tenant)}/ap-${upper(each.value.appname)}/epg-${upper(each.value.epg)}"
  tdn                 = "topology/pod-${each.value.podid}/paths-${tostring(each.value.leaf)}/pathep-[eth1/${tostring(each.value.port)}]"
  encap               = "vlan-${tostring(each.value.vlan)}"
  mode                = "untagged"
  instr_imedcy        = "immediate"
}

resource "aci_epg_to_static_path" "attach_epg_sp_native" {
  depends_on = [var.target_group_depends_on_3,var.target_group_depends_on_4]
  for_each            = {for attach_access in local.local_data_porttable_attach_sp_native : attach_access.pkey => attach_access}
  application_epg_dn  = "uni/tn-${upper(each.value.tenant)}/ap-${upper(each.value.appname)}/epg-${upper(each.value.epg)}"
  tdn                 = "topology/pod-${each.value.podid}/paths-${tostring(each.value.leaf)}/pathep-[eth1/${tostring(each.value.port)}]"
  encap               = "vlan-${tostring(each.value.vlan)}"
  mode                = "native"
  instr_imedcy        = "immediate"
}

resource "aci_epg_to_static_path" "attach_epg_sp_trunk" {
  depends_on = [var.target_group_depends_on_3,var.target_group_depends_on_4]
  for_each            = {for attach_access in local.local_data_porttable_attach_sp_trunk : attach_access.pkey => attach_access}
  application_epg_dn  = "uni/tn-${upper(each.value.tenant)}/ap-${upper(each.value.appname)}/epg-${upper(each.value.epg)}"
  tdn                 = "topology/pod-${each.value.podid}/paths-${tostring(each.value.leaf)}/pathep-[eth1/${tostring(each.value.port)}]"
  encap               = "vlan-${tostring(each.value.trunk_vlan)}"
  mode                = "regular"
  instr_imedcy        = "immediate"
}

resource "aci_epg_to_static_path" "attach_epg_vpc_access" {
  depends_on = [var.target_group_depends_on_3,var.target_group_depends_on_4]
  for_each            = {for attach_access in local.local_data_porttable_attach_vpc_access : attach_access.pkey => attach_access}
  application_epg_dn  = "uni/tn-${upper(each.value.tenant)}/ap-${upper(each.value.appname)}/epg-${upper(each.value.epg)}"
  tdn                 = "topology/pod-${each.value.podid}/protpaths-${tostring(each.value.leaf_vpc)}/pathep-[VPC_${upper(replace(each.value.device," ","_"))}_IPG]"
  encap               = "vlan-${tostring(each.value.vlan)}"
  mode                = "untagged"
  instr_imedcy        = "immediate"
}

resource "aci_epg_to_static_path" "attach_epg_vpc_native" {
  depends_on = [var.target_group_depends_on_3,var.target_group_depends_on_4]
  for_each            = {for attach_access in local.local_data_porttable_attach_vpc_native : attach_access.pkey => attach_access}
  application_epg_dn  = "uni/tn-${upper(each.value.tenant)}/ap-${upper(each.value.appname)}/epg-${upper(each.value.epg)}"
  tdn                 = "topology/pod-${each.value.podid}/protpaths-${tostring(each.value.leaf_vpc)}/pathep-[VPC_${upper(replace(each.value.device," ","_"))}_IPG]"
  encap               = "vlan-${tostring(each.value.vlan)}"
  mode                = "native"
  instr_imedcy        = "immediate"
}

resource "aci_epg_to_static_path" "attach_epg_vpc_trunk" {
  depends_on = [var.target_group_depends_on_3,var.target_group_depends_on_4]
  for_each            = {for attach_access in local.local_data_porttable_attach_vpc_trunk : attach_access.pkey => attach_access}
  application_epg_dn  = "uni/tn-${upper(each.value.tenant)}/ap-${upper(each.value.appname)}/epg-${upper(each.value.epg)}"
  tdn                 = "topology/pod-${each.value.podid}/protpaths-${tostring(each.value.leaf_vpc)}/pathep-[VPC_${upper(replace(each.value.device," ","_"))}_IPG]"
  encap               = "vlan-${tostring(each.value.trunk_vlan)}"
  mode                = "regular"
  instr_imedcy        = "immediate"
}

resource "aci_epg_to_static_path" "attach_epg_pc_access" {
  depends_on = [var.target_group_depends_on_3,var.target_group_depends_on_4]
  for_each            = {for attach_access in local.local_data_porttable_attach_pc_access : attach_access.pkey => attach_access}
  application_epg_dn  = "uni/tn-${upper(each.value.tenant)}/ap-${upper(each.value.appname)}/epg-${upper(each.value.epg)}"
  tdn                 = "topology/pod-${each.value.podid}/paths-${tostring(each.value.leaf_vpc)}/pathep-[PC_${upper(replace(each.value.device," ","_"))}_IPG]"
  encap               = "vlan-${tostring(each.value.vlan)}"
  mode                = "untagged"
  instr_imedcy        = "immediate"
}

resource "aci_epg_to_static_path" "attach_epg_pc_native" {
  depends_on = [var.target_group_depends_on_3,var.target_group_depends_on_4]
  for_each            = {for attach_access in local.local_data_porttable_attach_pc_native : attach_access.pkey => attach_access}
  application_epg_dn  = "uni/tn-${upper(each.value.tenant)}/ap-${upper(each.value.appname)}/epg-${upper(each.value.epg)}"
  tdn                 = "topology/pod-${each.value.podid}/paths-${tostring(each.value.leaf_vpc)}/pathep-[PC_${upper(replace(each.value.device," ","_"))}_IPG]"
  encap               = "vlan-${tostring(each.value.vlan)}"
  mode                = "native"
  instr_imedcy        = "immediate"
}

resource "aci_epg_to_static_path" "attach_epg_pc_trunk" {
  depends_on = [var.target_group_depends_on_3,var.target_group_depends_on_4]
  for_each            = {for attach_access in local.local_data_porttable_attach_pc_trunk : attach_access.pkey => attach_access}
  application_epg_dn  = "uni/tn-${upper(each.value.tenant)}/ap-${upper(each.value.appname)}/epg-${upper(each.value.epg)}"
  tdn                 = "topology/pod-${each.value.podid}/paths-${tostring(each.value.leaf_vpc)}/pathep-[PC_${upper(replace(each.value.device," ","_"))}_IPG]"
  encap               = "vlan-${tostring(each.value.trunk_vlan)}"
  mode                = "regular"
  instr_imedcy        = "immediate"
}
