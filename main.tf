# module "export" {
#    source = "./modules/export"
# }

module "tenant" {
   source = "./modules/tenant"
}

module "fabric" {
   source = "./modules/fabric"
}

module "l3out" {
  source = "./modules/l3out"
  target_group_depends_on_3 = module.tenant.eof_tenant_1
}

module "l3out_bgp_config" {
  source = "./modules/l3out_bgp_config"
  target_group_depends_on_5 = module.l3out.eof_l3out
}

module "attach_epg" {
  source = "./modules/attach_epg"
  target_group_depends_on_3 = module.tenant.eof_tenant_1
  target_group_depends_on_4 = module.fabric.eof_fabric_1
}