#!/bin/bash
#### build bash from create_view.sql file
#cat create_view.sql | grep "^CREATE VIEW" | sed 's/CREATE VIEW public.//g' | while read line ; do echo curl -s "http://10.249.250.124:3000/$line" -o $line.json ; done

#### Create local variable for terraform
#cat ./SKELETON/create_view.sql | grep "^CREATE VIEW" | sed 's/CREATE VIEW public.//g' | while read line ; do echo "local_data_$line = jsondecode(file(\"\${path.module}/$line.json\"))" ; done

# Remove old files
rm -f ipplan_*.json
rm -f porttable_*.json

# Export all data from views and save into json file
curl -s http://10.249.250.124:3000/ipplan_tenant -o ipplan_tenant.json
curl -s http://10.249.250.124:3000/ipplan_tenant_unique -o ipplan_tenant_unique.json
curl -s http://10.249.250.124:3000/ipplan_appname -o ipplan_appname.json
curl -s http://10.249.250.124:3000/ipplan_vrf -o ipplan_vrf.json
curl -s http://10.249.250.124:3000/ipplan_bridge_domain -o ipplan_bridge_domain.json
curl -s http://10.249.250.124:3000/ipplan_subnet_gateway -o ipplan_subnet_gateway.json
curl -s http://10.249.250.124:3000/ipplan_epg -o ipplan_epg.json
curl -s http://10.249.250.124:3000/ipplan_l3outconfiguration_global -o ipplan_l3outconfiguration_global.json
curl -s http://10.249.250.124:3000/ipplan_bgp_per_vrf -o ipplan_bgp_per_vrf.json
curl -s http://10.249.250.124:3000/ipplan_phy_vlan_pool -o ipplan_phy_vlan_pool.json
curl -s http://10.249.250.124:3000/ipplan_l3_vlan_pool -o ipplan_l3_vlan_pool.json
curl -s http://10.249.250.124:3000/ipplan_aaep -o ipplan_aaep.json
curl -s http://10.249.250.124:3000/porttable_vpc_groups -o porttable_vpc_groups.json
curl -s http://10.249.250.124:3000/porttable_vpc_policy_groups -o porttable_vpc_policy_groups.json
curl -s http://10.249.250.124:3000/porttable_pc_policy_groups -o porttable_pc_policy_groups.json
curl -s http://10.249.250.124:3000/porttable_sp_profile -o porttable_sp_profile.json
curl -s http://10.249.250.124:3000/porttable_pc_profile -o porttable_pc_profile.json
curl -s http://10.249.250.124:3000/porttable_vpc_profile -o porttable_vpc_profile.json
curl -s http://10.249.250.124:3000/porttable_switch_sp_pc_profile -o porttable_switch_sp_pc_profile.json
curl -s http://10.249.250.124:3000/porttable_switch_sp_pc_profile_detail -o porttable_switch_sp_pc_profile_detail.json
curl -s http://10.249.250.124:3000/porttable_sp_port_block -o porttable_sp_port_block.json
curl -s http://10.249.250.124:3000/porttable_pc_port_block -o porttable_pc_port_block.json
curl -s http://10.249.250.124:3000/porttable_vpc_port_block -o porttable_vpc_port_block.json
curl -s http://10.249.250.124:3000/porttable_port_description -o porttable_port_description.json
curl -s http://10.249.250.124:3000/porttable_attach_sp_access -o porttable_attach_sp_access.json
curl -s http://10.249.250.124:3000/porttable_attach_sp_native -o porttable_attach_sp_native.json
curl -s http://10.249.250.124:3000/porttable_attach_sp_trunk -o porttable_attach_sp_trunk.json
curl -s http://10.249.250.124:3000/porttable_attach_pc_access -o porttable_attach_pc_access.json
curl -s http://10.249.250.124:3000/porttable_attach_pc_native -o porttable_attach_pc_native.json
curl -s http://10.249.250.124:3000/porttable_attach_pc_trunk -o porttable_attach_pc_trunk.json
curl -s http://10.249.250.124:3000/porttable_attach_vpc_access -o porttable_attach_vpc_access.json
curl -s http://10.249.250.124:3000/porttable_attach_vpc_native -o porttable_attach_vpc_native.json
curl -s http://10.249.250.124:3000/porttable_attach_vpc_trunk -o porttable_attach_vpc_trunk.json



plan_file="${1}.plan"
if test -f "$plan_file"; then
    rm -f "$plan_file"
fi

#run_val="$(terraform plan -out "$plan_file")"
#echo $run_val

