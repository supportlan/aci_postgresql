terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

provider "aci" {
  username = var.username
  private_key = var.private_key
  cert_name = var.cert_name
  url      = var.url
  insecure = true
}